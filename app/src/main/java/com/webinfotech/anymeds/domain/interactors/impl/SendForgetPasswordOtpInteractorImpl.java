package com.webinfotech.anymeds.domain.interactors.impl;

import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.SendForgetPasswordOtpInteractor;
import com.webinfotech.anymeds.domain.interactors.base.AbstractInteractor;
import com.webinfotech.anymeds.domain.models.OtpResponse;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

public class SendForgetPasswordOtpInteractorImpl extends AbstractInteractor implements SendForgetPasswordOtpInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String mobile;

    public SendForgetPasswordOtpInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String mobile) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.mobile = mobile;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSendOtpFail(errorMsg);
            }
        });
    }

    private void postMessage(String otp){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSendOtpSuccess(otp);
            }
        });
    }

    @Override
    public void run() {
        final OtpResponse otpResponse = mRepository.sendForgetPasswordOtp(mobile);
        if (otpResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!otpResponse.status) {
            notifyError(otpResponse.message);
        } else {
            postMessage(otpResponse.otpData.otp);
        }
    }
}
