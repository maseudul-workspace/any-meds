package com.webinfotech.anymeds.domain.models.testing;

public class Image {

    public int id;
    public String image;

    public Image(int id, String image) {
        this.id = id;
        this.image = image;
    }
}
