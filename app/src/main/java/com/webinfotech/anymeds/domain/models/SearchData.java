package com.webinfotech.anymeds.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchData {

    @SerializedName("search_key")
    @Expose
    public String searchKey;

    @SerializedName("products")
    @Expose
    public Product[] products;

}
