package com.webinfotech.anymeds.domain.interactors;

import com.webinfotech.anymeds.domain.models.Pharmacy;

public interface FetchPharmacyListInteractor {
    interface Callback {
        void onGettingPharmacyListSuccess(Pharmacy[] pharmacies);
        void onGettingPharmacyListFail(String errorMsg);
    }
}
