package com.webinfotech.anymeds.domain.interactors;

import com.webinfotech.anymeds.domain.models.CartList;

public interface FetchCartListInteractor {
    interface Callback {
        void onGettingCartListSuccess(CartList[] cartLists);
        void onGettingCartListFail(String errorMsg, int loginError);
    }
}
