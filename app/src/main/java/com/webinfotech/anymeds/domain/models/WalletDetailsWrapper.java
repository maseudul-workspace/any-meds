package com.webinfotech.anymeds.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by Raj on 22-02-2019.
 */

public class WalletDetailsWrapper {

    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("total_page")
    @Expose
    public int totalPage;

    @SerializedName("login_error")
    @Expose
    public int login_error = 0;


    @SerializedName("data")
    @Expose
    public WalletDetails walletDetails;

}
