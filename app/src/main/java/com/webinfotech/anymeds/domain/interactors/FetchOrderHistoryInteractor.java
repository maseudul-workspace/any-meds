package com.webinfotech.anymeds.domain.interactors;

import com.webinfotech.anymeds.domain.models.Orders;

public interface FetchOrderHistoryInteractor {
    interface Callback {
        void onGettingOrderHistorySuccess(Orders[] orders, int totalPage);
        void onGettingOrderHistoryFail(String errorMsg, int loginError);
    }
}
