package com.webinfotech.anymeds.domain.interactors;

import com.webinfotech.anymeds.domain.models.Subcategory;

public interface FetchSubcategoriesInteractor {
    interface Callback {
        void onGettingSubcategoriesSuccess(Subcategory[] subcategories);
        void onGettingSubcategoriesFail(String errorMsg);
    }
}
