package com.webinfotech.anymeds.domain.interactors;


import com.webinfotech.anymeds.domain.models.PaymentRequestResponse;

public interface RequestPaymentInteractor {
    interface Callback {
        void onRequestPaymentSuccess(PaymentRequestResponse paymentRequestResponse);
        void onRequestPaymentFail(String errorMsg);
    }
}
