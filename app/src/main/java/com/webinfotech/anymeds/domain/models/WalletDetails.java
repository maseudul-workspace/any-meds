package com.webinfotech.anymeds.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 22-02-2019.
 */

public class WalletDetails {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("wallet_balance")
    @Expose
    public double totalAmount;

    @SerializedName("history")
    @Expose
    public WalletHistory[] walletHistories;

}
