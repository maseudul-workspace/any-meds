package com.webinfotech.anymeds.domain.interactors;

public interface RemoveCartItemInteractor {
    interface Callback {
        void onCartItemRemoveSuccess();
        void onCartItemRemoveFail(String errorMsg, int loginError);
    }
}
