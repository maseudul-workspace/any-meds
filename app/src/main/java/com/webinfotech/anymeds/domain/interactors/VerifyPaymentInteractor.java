package com.webinfotech.anymeds.domain.interactors;

public interface VerifyPaymentInteractor {
    interface Callback {
        void onVerifyPaymentSuccess();
        void onVerifyPaymentFail();
    }
}
