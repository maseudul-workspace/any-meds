package com.webinfotech.anymeds.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeData {

    @SerializedName("category_products")
    @Expose
    public CategoryProducts[] categoryProducts;

    @SerializedName("popular_products")
    @Expose
    public Product[] popularProducts;

    @SerializedName("pharmacy")
    @Expose
    public Pharmacy[] pharmacies;

    @SerializedName("banners")
    @Expose
    public Banners banners;

}
