package com.webinfotech.anymeds.domain.interactors;

import com.webinfotech.anymeds.domain.models.UserInfo;

public interface CheckLoginInteractor {
    interface Callback {
        void onLoginSuccess(UserInfo userInfo);
        void onLoginFail(String errorMsg);
    }
}
