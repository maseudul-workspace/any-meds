package com.webinfotech.anymeds.domain.interactors;

import com.webinfotech.anymeds.domain.models.WishList;

public interface FetchWishListInteractor {
    interface Callback {
        void onWishListFetchSuccess(WishList[] wishLists);
        void onWishListFetchFail(String errorMsg, int loginError);
    }
}
