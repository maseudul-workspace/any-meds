package com.webinfotech.anymeds.domain.interactors;


import com.webinfotech.anymeds.domain.models.ShippingAddress;

public interface FetchShippingAddressDetailsInteractor {
    interface Callback {
        void onGettingAddressDetailsSuccess(ShippingAddress shippingAddress);
        void onGettingAddressDetailsFail(String errorMsg, int loginError);
    }
}
