package com.webinfotech.anymeds.domain.interactors;


import com.webinfotech.anymeds.domain.models.Product;

public interface FetchProductListInteractor {
    interface Callback {
        void onGettingProductListSuccess(Product[] products, int totalPage);
        void onGettingProductListFail(String errorMsg);
    }
}
