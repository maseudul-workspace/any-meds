package com.webinfotech.anymeds.domain.interactors;

import com.webinfotech.anymeds.domain.models.ProductDetailsData;

public interface FetchProductDetailsInteractor {
    interface Callback {
        void onGettingProductDetailsSuccess(ProductDetailsData productDetailsData);
        void onGettingProductDetailsFail(String errorMsg);
    }
}
