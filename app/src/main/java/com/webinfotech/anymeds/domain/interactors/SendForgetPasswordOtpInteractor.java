package com.webinfotech.anymeds.domain.interactors;

public interface SendForgetPasswordOtpInteractor {
    interface Callback {
        void onSendOtpSuccess(String otp);
        void onSendOtpFail(String errorMsg);
    }
}
