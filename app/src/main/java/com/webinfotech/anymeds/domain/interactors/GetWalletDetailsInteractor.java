package com.webinfotech.anymeds.domain.interactors;


import com.webinfotech.anymeds.domain.models.WalletDetails;

public interface GetWalletDetailsInteractor {
    interface Callback {
        void onGettingCreditHistorySuccess(WalletDetails walletDetails);
        void onGettingCreditHistoryFail(String errorMsg, int loginError);
    }
}
