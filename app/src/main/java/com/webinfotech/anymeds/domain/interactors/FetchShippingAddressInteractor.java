package com.webinfotech.anymeds.domain.interactors;

import com.webinfotech.anymeds.domain.models.ShippingAddress;

public interface FetchShippingAddressInteractor {
    interface Callback {
        void onGettingAddressListSucces(ShippingAddress[] shippingAddresses);
        void onGettingAddressListFail(String errorMsg, int loginError);
    }
}
