package com.webinfotech.anymeds.domain.interactors.impl;

import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.ClaimIncentiveInteractor;
import com.webinfotech.anymeds.domain.interactors.base.AbstractInteractor;
import com.webinfotech.anymeds.domain.models.CommonResponse;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

public class ClaimIncentiveInteractorImpl extends AbstractInteractor implements ClaimIncentiveInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String cspId;
    String phoneNo;
    String incentive;

    public ClaimIncentiveInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String cspId, String phoneNo, String incentive) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.cspId = cspId;
        this.phoneNo = phoneNo;
        this.incentive = incentive;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onIncentiveClaimFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onIncentiveClaimSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.claimIncentive(cspId, phoneNo, incentive);
        if (commonResponse == null) {
            notifyError("Slow Internet Connection");
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message);
        } else {
            postMessage();
        }
    }
}
