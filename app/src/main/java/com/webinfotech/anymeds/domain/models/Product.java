package com.webinfotech.anymeds.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("main_image")
    @Expose
    public String mainImage;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("min_price")
    @Expose
    public double minPrice;

    @SerializedName("mrp")
    @Expose
    public double mrp;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("is_popular")
    @Expose
    public int isPopular;

    @SerializedName("is_prescription")
    @Expose
    public int isPrescription;

    @SerializedName("sizes")
    @Expose
    public Size[] sizes;

    @SerializedName("sizes_count")
    @Expose
    public int sizesCount;

    @SerializedName("images")
    @Expose
    public Image[] images;

    public boolean isWishListed = false;

}
