package com.webinfotech.anymeds.domain.interactors.impl;

import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.SetTransactionIdInteractor;
import com.webinfotech.anymeds.domain.interactors.base.AbstractInteractor;
import com.webinfotech.anymeds.domain.models.CommonResponse;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

public class SetTransactionIdInteractorImpl extends AbstractInteractor implements SetTransactionIdInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int orderId;
    String transactionId;

    public SetTransactionIdInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallbackk, String apiToken, int orderId, String transactionId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallbackk;
        this.apiToken = apiToken;
        this.orderId = orderId;
        this.transactionId = transactionId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSetTransactionIdFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSetTransactionIdSuccess();
            }
        });
    }

    @Override
    public void run() {
        CommonResponse commonResponse = mRepository.setPaymentTransactionId(apiToken, orderId, transactionId);
        if (commonResponse == null) {
            notifyError("", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
