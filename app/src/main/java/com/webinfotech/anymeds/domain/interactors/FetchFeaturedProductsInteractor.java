package com.webinfotech.anymeds.domain.interactors;

import com.webinfotech.anymeds.domain.models.Product;

public interface FetchFeaturedProductsInteractor {
    interface Callback {
        void onFeaturedProductsFetchSuccess(Product[] products);
        void onFeaturedProductsFetchFail(String errorMsg);
    }
}
