package com.webinfotech.anymeds.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 22-02-2019.
 */

public class WalletHistory {

    @SerializedName("transaction_type")
    @Expose
    public int transaction_type;

    @SerializedName("amount")
    @Expose
    public double amount;

    @SerializedName("total_amount")
    @Expose
    public double totalAmount;

    @SerializedName("created_at")
    @Expose
    public String date;

}
