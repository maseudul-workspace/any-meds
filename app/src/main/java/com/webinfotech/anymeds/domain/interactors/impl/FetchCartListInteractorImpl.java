package com.webinfotech.anymeds.domain.interactors.impl;


import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.FetchCartListInteractor;
import com.webinfotech.anymeds.domain.interactors.base.AbstractInteractor;
import com.webinfotech.anymeds.domain.models.CartList;
import com.webinfotech.anymeds.domain.models.CartListWrapper;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

public class FetchCartListInteractorImpl extends AbstractInteractor implements FetchCartListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchCartListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int isLoginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCartListFail(errorMsg, isLoginError);
            }
        });
    }

    private void postMessage(CartList[] cartLists){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCartListSuccess(cartLists);
            }
        });
    }

    @Override
    public void run() {
        final CartListWrapper cartListWrapper = mRepository.fetchCartList(apiToken, userId);
        if (cartListWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!cartListWrapper.status) {
            notifyError(cartListWrapper.message, cartListWrapper.login_error);
        } else {
            postMessage(cartListWrapper.cartLists);
        }
    }
}
