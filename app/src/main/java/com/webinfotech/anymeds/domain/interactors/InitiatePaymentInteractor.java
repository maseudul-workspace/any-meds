package com.webinfotech.anymeds.domain.interactors;


import com.webinfotech.anymeds.domain.models.GatewayOrder;

public interface InitiatePaymentInteractor {
    interface Callback {
        void onInitiatePaymentSuccess(GatewayOrder gatewayOrder);
        void onInitiatePaymentFail(String errorMsg);
    }
}
