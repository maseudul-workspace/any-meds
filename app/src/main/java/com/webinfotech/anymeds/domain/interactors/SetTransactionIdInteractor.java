package com.webinfotech.anymeds.domain.interactors;

public interface SetTransactionIdInteractor {
    interface Callback {
        void onSetTransactionIdSuccess();
        void onSetTransactionIdFail(String errorMsg, int loginError);
    }
}
