package com.webinfotech.anymeds.domain.interactors.impl;
import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.InitiatePaymentInteractor;
import com.webinfotech.anymeds.domain.interactors.base.AbstractInteractor;
import com.webinfotech.anymeds.domain.models.GatewayOrder;
import com.webinfotech.anymeds.domain.models.GatewayOrderResponse;
import com.webinfotech.anymeds.repository.PaymentRepositoryImpl;

public class InitiatePaymentInteractorImpl extends AbstractInteractor implements InitiatePaymentInteractor {

    PaymentRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    String name;
    String email;
    String phone;
    String amount;
    String description;
    String currency;
    String transaction_id;
    String redirect_url;
    String env;

    public InitiatePaymentInteractorImpl(Executor threadExecutor, MainThread mainThread, PaymentRepositoryImpl mRepository, Callback mCallback, String authorization, String name, String email, String phone, String amount, String description, String currency, String transaction_id, String redirect_url, String env) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.amount = amount;
        this.description = description;
        this.currency = currency;
        this.transaction_id = transaction_id;
        this.redirect_url = redirect_url;
        this.env = env;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onInitiatePaymentFail(errorMsg);
            }
        });
    }

    private void postMessage(GatewayOrder gatewayOrder){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onInitiatePaymentSuccess(gatewayOrder);
            }
        });
    }

    @Override
    public void run() {
        final GatewayOrderResponse gatewayOrderResponse = mRepository.initiateOrder(authorization, name, email, phone, amount, description, currency, transaction_id, redirect_url, env);
        if (gatewayOrderResponse ==  null) {
            notifyError("Something went wrong");
        } else if (!gatewayOrderResponse.success) {
            notifyError(gatewayOrderResponse.message);
        } else {
            postMessage(gatewayOrderResponse.gatewayOrder);
        }
    }
}
