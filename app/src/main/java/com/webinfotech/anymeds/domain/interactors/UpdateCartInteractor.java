package com.webinfotech.anymeds.domain.interactors;

public interface UpdateCartInteractor {
    interface Callback {
        void onCartUpdateSuccess();
        void onCartUpdateFail(String errorMsg, int loginError);
    }
}
