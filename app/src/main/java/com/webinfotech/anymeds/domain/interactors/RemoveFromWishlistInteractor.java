package com.webinfotech.anymeds.domain.interactors;

public interface RemoveFromWishlistInteractor {
    interface Callback {
        void onWishListRemoveSuccess();
        void onWishListRemoveFail(String errorMsg, int loginError);
    }
}
