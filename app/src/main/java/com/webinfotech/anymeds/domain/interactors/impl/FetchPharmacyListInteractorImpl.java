package com.webinfotech.anymeds.domain.interactors.impl;

import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.FetchPharmacyListInteractor;
import com.webinfotech.anymeds.domain.interactors.base.AbstractInteractor;
import com.webinfotech.anymeds.domain.models.Pharmacy;
import com.webinfotech.anymeds.domain.models.PharmacyListWrapper;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

public class FetchPharmacyListInteractorImpl extends AbstractInteractor implements FetchPharmacyListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;

    public FetchPharmacyListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingPharmacyListFail(errorMsg);
            }
        });
    }

    private void postMessage(Pharmacy[] pharmacies){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingPharmacyListSuccess(pharmacies);
            }
        });
    }

    @Override
    public void run() {
        final PharmacyListWrapper pharmacyListWrapper = mRepository.fetchPharmacyList();
        if (pharmacyListWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!pharmacyListWrapper.status) {
            notifyError(pharmacyListWrapper.message);
        } else {
            postMessage(pharmacyListWrapper.pharmacies);
        }
    }
}
