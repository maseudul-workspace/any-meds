package com.webinfotech.anymeds.domain.interactors;


import com.webinfotech.anymeds.domain.models.UserInfo;

public interface FetchUserProfileInteractor {
    interface Callback {
        void onGettingUserProfileSuccess(UserInfo userInfo);
        void onGettingUserProfileFail(String errorMsg, int loginError);
    }
}
