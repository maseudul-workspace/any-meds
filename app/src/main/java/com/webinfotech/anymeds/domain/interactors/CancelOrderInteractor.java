package com.webinfotech.anymeds.domain.interactors;

public interface CancelOrderInteractor {
    interface Callback {
        void onOrderCancelSuccess();
        void onOrderCancelFail(String errorMsg, int loginError);
    }
}
