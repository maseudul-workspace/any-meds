package com.webinfotech.anymeds.domain.interactors.impl;

import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.PlaceOrderInteractor;
import com.webinfotech.anymeds.domain.interactors.base.AbstractInteractor;
import com.webinfotech.anymeds.domain.models.OrderPlaceData;
import com.webinfotech.anymeds.domain.models.OrderPlaceDataResponse;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

public class PlaceOrderInteractorImpl extends AbstractInteractor implements PlaceOrderInteractor {

    AppRepositoryImpl mRespository;
    Callback mCallback;
    String apitoken;
    int userId;
    int paymentMethod;
    int addressId;
    int orderType;
    int isWallet;
    int pharmacyId;
    String pharmacyName;
    String prescription;

    public PlaceOrderInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRespository, Callback mCallback, String apitoken, int userId, int paymentMethod, int addressId, int orderType, int isWallet, int pharmacyId, String pharmacyName, String prescription) {
        super(threadExecutor, mainThread);
        this.mRespository = mRespository;
        this.mCallback = mCallback;
        this.apitoken = apitoken;
        this.userId = userId;
        this.paymentMethod = paymentMethod;
        this.addressId = addressId;
        this.orderType = orderType;
        this.isWallet = isWallet;
        this.prescription = prescription;
        this.pharmacyId = pharmacyId;
        this.pharmacyName = pharmacyName;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPlaceOrderFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(OrderPlaceData orderPlaceData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPlaceOrderSuccess(orderPlaceData);
            }
        });
    }

    @Override
    public void run() {
        final OrderPlaceDataResponse orderPlaceDataResponse = mRespository.placeOrder(apitoken, userId, paymentMethod, addressId, orderType, isWallet, pharmacyId, pharmacyName, prescription);
        if (orderPlaceDataResponse == null) {
            notifyError("Something went wrong", 0);
        } else if (!orderPlaceDataResponse.status) {
            notifyError(orderPlaceDataResponse.message, orderPlaceDataResponse.login_error);
        } else {
            postMessage(orderPlaceDataResponse.orderPlaceData);
        }
    }
}
