package com.webinfotech.anymeds.domain.interactors;

public interface UpdateUserInteractor {
    interface Callback {
        void onUserUpdateSuccess();
        void onUserUpdateFail(String errorMsg, int loginError);
    }
}
