package com.webinfotech.anymeds.domain.interactors;

public interface RegisterUserInteractor {
    interface Callback {
        void onRegisterSuccess();
        void onRegisterFail(String errorMsg);
    }
}
