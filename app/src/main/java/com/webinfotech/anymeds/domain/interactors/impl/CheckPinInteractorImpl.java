package com.webinfotech.anymeds.domain.interactors.impl;

import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.CheckPinInteractor;
import com.webinfotech.anymeds.domain.interactors.base.AbstractInteractor;
import com.webinfotech.anymeds.domain.models.CommonResponse;
import com.webinfotech.anymeds.domain.models.UserInfo;
import com.webinfotech.anymeds.repository.AppRepository;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

public class CheckPinInteractorImpl extends AbstractInteractor implements CheckPinInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String pin;

    public CheckPinInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String pin) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.pin = pin;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPinCodeNotAvailable(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPinCodeAvailable();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.checkPin(pin);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!commonResponse.status) {
            notifyError("Sorry Delivery Not Available");
        } else {
            postMessage();
        }
    }
}
