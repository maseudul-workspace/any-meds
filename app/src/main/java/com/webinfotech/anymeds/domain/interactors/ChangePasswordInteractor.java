package com.webinfotech.anymeds.domain.interactors;

public interface ChangePasswordInteractor {
    interface Callback {
        void onPasswordChangeSuccess();
        void onPasswordChangeFail(String errorMsg, int loginError);
    }
}
