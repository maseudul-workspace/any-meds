package com.webinfotech.anymeds.domain.interactors.impl;

import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.FetchHomeDataInteractor;
import com.webinfotech.anymeds.domain.interactors.base.AbstractInteractor;
import com.webinfotech.anymeds.domain.models.HomeData;
import com.webinfotech.anymeds.domain.models.HomeDataWrapper;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

public class FetchHomeDataInteractorImpl extends AbstractInteractor implements FetchHomeDataInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;

    public FetchHomeDataInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingHomeDataFail(errorMsg);
            }
        });
    }

    private void postMessage(HomeData homeData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingHomeDataSuccess(homeData);
            }
        });
    }

    @Override
    public void run() {
        final HomeDataWrapper homeDataWrapper = mRepository.fetchHomeData();
        if (homeDataWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!homeDataWrapper.status) {
            notifyError(homeDataWrapper.message);
        } else {
            postMessage(homeDataWrapper.homeData);
        }
    }
}
