package com.webinfotech.anymeds.domain.interactors;

import com.webinfotech.anymeds.domain.models.HomeData;

public interface FetchHomeDataInteractor {
    interface Callback {
        void onGettingHomeDataSuccess(HomeData homeData);
        void onGettingHomeDataFail(String errorMsg);
    }
}
