package com.webinfotech.anymeds.domain.interactors;

public interface RegisterCspInteractor {
    interface Callback {
        void onRegisterCspSuccess();
        void onRegisterCspFail(String errorMsg);
    }
}
