package com.webinfotech.anymeds.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderProducts {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("product_id")
    @Expose
    public int productId;

    @SerializedName("product_name")
    @Expose
    public String productName;

    @SerializedName("product_type")
    @Expose
    public int product_type;

    @SerializedName("product_image")
    @Expose
    public String productImage;

    @SerializedName("size")
    @Expose
    public String size;

    @SerializedName("quantity")
    @Expose
    public String quantity;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("mrp")
    @Expose
    public String mrp;

}
