package com.webinfotech.anymeds.domain.interactors.impl;

import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.FetchChargesListInteractor;
import com.webinfotech.anymeds.domain.interactors.base.AbstractInteractor;
import com.webinfotech.anymeds.domain.models.Charges;
import com.webinfotech.anymeds.domain.models.ChargesListWrapper;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

public class FetchChargesListInteractorImpl extends AbstractInteractor implements FetchChargesListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;

    public FetchChargesListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onChargesListFetchFail();
            }
        });
    }

    private void postMessage(Charges[] charges){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onChargesListFetchSuccess(charges);
            }
        });
    }

    @Override
    public void run() {
        ChargesListWrapper chargesListWrapper = mRepository.fetchChargesList();
        if (chargesListWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!chargesListWrapper.status) {
            notifyError("Please Check Your Internet Connection");
        } else {
            postMessage(chargesListWrapper.charges);
        }
    }
}
