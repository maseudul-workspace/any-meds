package com.webinfotech.anymeds.domain.interactors;

import com.webinfotech.anymeds.domain.models.Product;

public interface FetchSearchListInteractor {
    interface Callback {
        void onGettingSearchListSuccess(Product[] products, String searchKey);
        void onGettingSearchListFail(String errorMsg);
    }
}
