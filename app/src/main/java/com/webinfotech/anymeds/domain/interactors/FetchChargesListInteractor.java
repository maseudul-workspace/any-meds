package com.webinfotech.anymeds.domain.interactors;


import com.webinfotech.anymeds.domain.models.Charges;

public interface FetchChargesListInteractor {
    interface Callback {
        void onChargesListFetchSuccess(Charges[] charges);
        void onChargesListFetchFail();
    }
}
