package com.webinfotech.anymeds.domain.interactors;


import com.webinfotech.anymeds.domain.models.OrderPlaceData;

public interface PlaceOrderInteractor {
    interface Callback {
        void onPlaceOrderSuccess(OrderPlaceData orderPlaceData);
        void onPlaceOrderFail(String errorMsg, int loginError);
    }
}
