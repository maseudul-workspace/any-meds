package com.webinfotech.anymeds.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface PaymentRepository {

    @POST("v2/gateway/orders/")
    @FormUrlEncoded
    Call<ResponseBody> initiateOrder(@Header("Authorization") String authorization,
                                     @Field("name") String name,
                                     @Field("email") String email,
                                     @Field("phone") String phone,
                                     @Field("amount") String amount,
                                     @Field("description") String description,
                                     @Field("currency") String currency,
                                     @Field("transaction_id") String transaction_id,
                                     @Field("redirect_url") String redirect_url,
                                     @Field("env") String env
    );

    @POST("v2/gateway/orders/payment-request/")
    @FormUrlEncoded
    Call<ResponseBody> requestPayment(@Header("Authorization") String authorization,
                                      @Field("id") String id);

}
