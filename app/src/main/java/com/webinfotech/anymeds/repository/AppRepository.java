package com.webinfotech.anymeds.repository;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface AppRepository {

    @GET("app/load/")
    Call<ResponseBody> fetchHomeData();

    @GET("sub/category/list/{category_id}")
    Call<ResponseBody> fetchSubcategories(@Path("category_id") int categoryId);

    @GET("product/list/{category_id}/{type}/{page}")
    Call<ResponseBody> fetchProductList(@Path("category_id") int categoryId,
                                        @Path("type") int type,
                                        @Path("page") int page
    );

    @GET("product/product/single/view/{product_id}")
    Call<ResponseBody> fetchProductDetails(@Path("product_id") int productId);

    @POST("user/registration")
    @FormUrlEncoded
    Call<ResponseBody> registerUser(@Field("name") String name,
                                    @Field("email") String email,
                                    @Field("mobile") String mobile,
                                    @Field("state") String state,
                                    @Field("city") String city,
                                    @Field("pin") String pin,
                                    @Field("address") String address,
                                    @Field("password") String password,
                                    @Field("confirm_password") String confirmPassword
    );

    @GET("send/otp/{mobile}")
    Call<ResponseBody> sendOtp(@Path("mobile") String mobile);

    @POST("user/login")
    @FormUrlEncoded
    Call<ResponseBody> checkLogin(@Field("user_id") String userId,
                                  @Field("password") String password
    );

    @POST("user/shipping/add")
    @FormUrlEncoded
    Call<ResponseBody> addShippingAddress(@Header("Authorization") String authorization,
                                          @Field("user_id") int userId,
                                          @Field("name") String name,
                                          @Field("email") String email,
                                          @Field("mobile") String mobile,
                                          @Field("state") String state,
                                          @Field("city") String city,
                                          @Field("pin") String pin,
                                          @Field("address") String address
    );

    @GET("user/shipping/single/{user_id}/{address_id}")
    Call<ResponseBody> fetchShippingAddressDetails(@Header("Authorization") String authorization,
                                                   @Path("user_id") int userId,
                                                   @Path("address_id") int addressId
    );


    @GET("user/shipping/list/{user_id}")
    Call<ResponseBody> fetchShippingAddressList(@Header("Authorization") String authorization,
                                                @Path("user_id") int userId
    );

    @POST("user/shipping/update")
    @FormUrlEncoded
    Call<ResponseBody> updateShippingAddress(@Header("Authorization") String authorization,
                                             @Field("user_id") int userId,
                                             @Field("address_id") int addressId,
                                             @Field("name") String name,
                                             @Field("email") String email,
                                             @Field("mobile") String mobile,
                                             @Field("state") String state,
                                             @Field("city") String city,
                                             @Field("pin") String pin,
                                             @Field("address") String address
    );

    @GET("user/shipping/delete/{address_id}")
    Call<ResponseBody> deleteShippingAddress(@Header("Authorization") String authorization,
                                             @Path("address_id") int addressId
    );

    @GET("user/profile/{user_id}")
    Call<ResponseBody> fetchUserProfile(@Header("Authorization") String authorization,
                                        @Path("user_id") int userId
    );

    @POST("user/profile/update")
    @FormUrlEncoded
    Call<ResponseBody> updateUser(  @Header("Authorization") String authorization,
                                    @Field("user_id") int userId,
                                    @Field("name") String name,
                                    @Field("email") String email,
                                    @Field("mobile") String mobile,
                                    @Field("state") String state,
                                    @Field("city") String city,
                                    @Field("pin") String pin,
                                    @Field("address") String address
    );

    @POST("user/change/password")
    @FormUrlEncoded
    Call<ResponseBody> changePassword(@Header("Authorization") String authorization,
                                      @Field("user_id") int userId,
                                      @Field("current_pass") String currentPassword,
                                      @Field("new_password") String newPassword,
                                      @Field("confirm_password") String confirmPassword
    );

    @GET("user/wish/list/items/{user_id}")
    Call<ResponseBody> fetchWishList(@Header("Authorization") String authorization,
                                     @Path("user_id") int userId
    );

    @GET("user/wish/list/add/{product_id}/{user_id}")
    Call<ResponseBody> addToWishList(@Header("Authorization") String authorization,
                                     @Path("user_id") int userId,
                                     @Path("product_id") int productId
    );

    @GET("user/wish/list/item/remove/{wish_list_id}")
    Call<ResponseBody> removeFromWishList(  @Header("Authorization") String authorization,
                                            @Path("wish_list_id") int wishListId
    );

    @POST("user/cart/add")
    @FormUrlEncoded
    Call<ResponseBody> addToCart(@Header("Authorization") String authorization,
                                 @Field("user_id") int userId,
                                 @Field("product_id") int productId,
                                 @Field("quantity") String qty,
                                 @Field("size_id") String sizeId
    );

    @GET("user/cart/fetch/product/{user_id}")
    Call<ResponseBody> fetchCartList(@Header("Authorization") String authorization,
                                     @Path("user_id") int user_id);

    @GET("user/cart/update/quantity/{cart_id}/{quantity}")
    Call<ResponseBody> updateCart(@Header("Authorization") String authorization,
                                  @Path("cart_id") int cartId,
                                  @Path("quantity") int quantity
    );

    @GET("user/cart/remove/item/{cart_id}")
    Call<ResponseBody> deleteCart(@Header("Authorization") String authorization,
                                  @Path("cart_id") int cartId
    );

    @GET("charges/list")
    Call<ResponseBody> fetchChargesList();

    @GET("user/wallet/fetch/{user_id}")
    Call<ResponseBody> fetchWalletDetails(@Header("Authorization") String authorization,
                                          @Path("user_id") int user_id);

    @GET("product/medecine/search/{search_key}")
    Call<ResponseBody> fetchSearchList(@Path("search_key") String search_key);

    @POST("user/place/order")
    @Multipart
    Call<ResponseBody> placeOrder(   @Header("Authorization") String authorization,
                                       @Part("user_id") RequestBody userId,
                                       @Part("payment_type") RequestBody paymentType,
                                       @Part("shipping_address_id") RequestBody shippingAddressId,
                                       @Part("order_type") RequestBody orderType,
                                       @Part("is_wallet") RequestBody isWallet,
                                     @Part("seller_pharmacy_id") RequestBody pharmacyId,
                                     @Part("seller_pharmacy_name") RequestBody pharmacyName,
                                     @Part MultipartBody.Part prescription
                                       );

    @GET("user/order/history/{user_id}/{page}")
    Call<ResponseBody> fetchOrderList(  @Header("Authorization") String authorization,
                                        @Path("user_id") int userId,
                                        @Path("page") int page
    );

    @POST("user/order/payment/request/id/update")
    @FormUrlEncoded
    Call<ResponseBody> requestPaymentId(  @Header("Authorization") String authorization,
                                          @Field("order_id") int orderId,
                                          @Field("payment_request_id") String paymentRequestId
    );

    @POST("user/order/payment/verify")
    @FormUrlEncoded
    Call<ResponseBody> verifyPayment(  @Header("Authorization") String authorization,
                                       @Field("user_id") int userId,
                                       @Field("order_id") int orderId,
                                       @Field("instamozo_order_id") String instamozoOrderId
    );

    @GET("user/order/cancel/{order_id}")
    Call<ResponseBody> cancelOrder(  @Header("Authorization") String authorization,
                                     @Path("order_id") int orderId
    );

    @POST("csp/register")
    @FormUrlEncoded
    Call<ResponseBody> registerCsp(  @Field("name") String name,
                                     @Field("pharmacy_name") String pharmacyName,
                                     @Field("phone") String phoneNo,
                                     @Field("phone_pay_no") String phonePayNo,
                                     @Field("pin") String pin,
                                     @Field("address") String address,
                                     @Field("bank") String bank,
                                     @Field("ifsc") String ifsc,
                                     @Field("email") String email
                                     );

    @POST("seller/register")
    @FormUrlEncoded
    Call<ResponseBody> registerSeller(   @Field("pharmacy_name") String name,
                                         @Field("dl_no") String dl_no,
                                         @Field("gst_no") String gst_no,
                                         @Field("owner_name") String owner_name,
                                         @Field("owner_no") String owner_no,
                                         @Field("whatsapp_No") String whatsapp_No,
                                         @Field("phone_pay_no") String phone_pay_no,
                                         @Field("email") String email,
                                         @Field("pin") String pin,
                                         @Field("address") String address,
                                         @Field("bank_no") String bank_no,
                                         @Field("ifsc") String ifsc
                                         );

    @POST("incentive/claim")
    @Multipart
    Call<ResponseBody> claimIncentive(   @Part("csp_id") RequestBody cspId,
                                         @Part("phone_no") RequestBody phone_no,
                                         @Part MultipartBody.Part incentiveBasis
    );

    @POST("user/order/history/payment")
    @FormUrlEncoded
    Call<ResponseBody> payFromOrderHistory(  @Header("Authorization") String authorization,
                                             @Field("order_id") int orderId
    );

    @GET("top/pharmacy/list")
    Call<ResponseBody> fetchPharmacyList();

    @GET("featured/product/list")
    Call<ResponseBody> fetchFeaturedProducts();

    @GET("send/otp/forget/pass/{mobile}")
    Call<ResponseBody> sendForgetPasswordOtp(@Path("mobile") String mobile);

    @POST("user/forgot/change/password")
    @FormUrlEncoded
    Call<ResponseBody> resetPassword(   @Field("mobile") String mobile,
                                        @Field("new_password") String password,
                                        @Field("confirm_password") String confirmPassword
    );

    @POST("product/check/pin/code")
    @FormUrlEncoded
    Call<ResponseBody> checkPin(@Field("pin") String pin);

}
