package com.webinfotech.anymeds.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.TooltipCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.instamojo.android.Instamojo;
import com.skydoves.balloon.ArrowOrientation;
import com.skydoves.balloon.Balloon;
import com.skydoves.balloon.BalloonAnimation;
import com.webinfotech.anymeds.R;
import com.webinfotech.anymeds.domain.executors.impl.ThreadExecutor;
import com.webinfotech.anymeds.domain.models.Charges;
import com.webinfotech.anymeds.domain.models.OrderPlaceData;
import com.webinfotech.anymeds.presentation.presenters.CartDetailsPresenter;
import com.webinfotech.anymeds.presentation.presenters.impl.CartDetailsPresenterImpl;
import com.webinfotech.anymeds.presentation.ui.adapters.CartListAdapter;
import com.webinfotech.anymeds.presentation.ui.adapters.CartShippingAddressAdapter;
import com.webinfotech.anymeds.presentation.ui.adapters.DeliveryChargesAdapter;
import com.webinfotech.anymeds.presentation.ui.bottomsheets.LoginBottomSheet;
import com.webinfotech.anymeds.presentation.ui.dialogs.PaymentOptionDialog;
import com.webinfotech.anymeds.threading.MainThreadImpl;
import com.webinfotech.anymeds.util.GlideHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.webinfotech.anymeds.util.Helper.calculateFileSize;
import static com.webinfotech.anymeds.util.Helper.getRealPathFromURI;
import static com.webinfotech.anymeds.util.Helper.saveImage;

public class CartListActivity extends AppCompatActivity implements CartDetailsPresenter.View, PaymentOptionDialog.Callback, Instamojo.InstamojoPaymentCallback {

    @BindView(R.id.recycler_view_cart_list)
    RecyclerView recyclerViewCartList;
    @BindView(R.id.main_layout)
    View mainLayout;
    @BindView(R.id.txt_view_sub_total)
    TextView txtViewSubTotal;
    @BindView(R.id.txt_view_total)
    TextView txtViewTotal;
    @BindView(R.id.txt_view_discount)
    TextView txtViewDiscount;
    BottomSheetDialog addressBottomSheetDialog;
    TextView txtViewAddAddress;
    ImageView imgViewAddAddress;
    RecyclerView recyclerViewShippingAddress;
    CartDetailsPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    Charges[] charges;
    double totalMrp;
    double totalAmount;
    double discount;
    double deliveryCharges;
    int shippingAddressId;
    int deliveryType;
    @BindView(R.id.img_view_exclamation)
    ImageView imgViewExclamation;
    Balloon balloon;
    @BindView(R.id.txt_view_delivery_charges)
    TextView txtViewDeliveryCharges;
    @BindView(R.id.layout_wallet_pay)
    View layoutWalletPay;
    @BindView(R.id.checkbox_wallet)
    CheckBox walletCheckbox;
    double chargeAmount;
    double walletAmount;
    String[] appPremisions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;
    private static final int REQUEST_PIC = 1000;
    String filePath;
    @BindView(R.id.img_view_prescription)
    ImageView imgViewPrescription;
    @BindView(R.id.layout_prescription)
    View layoutPrescription;
    @BindView(R.id.layout_prescription_main)
    View layoutPrescriptionMain;
    @BindView(R.id.txt_view_without_prescription)
    TextView txtViewWithoutPrescription;
    @BindView(R.id.radio_btn_with_prescription)
    RadioButton radioBtnPrescription;
    @BindView(R.id.radio_btn_without_prescription)
    RadioButton radioBtnWithoutPrescription;
    private static final HashMap<Instamojo.Environment, String> env_options = new HashMap<>();
    static {
        env_options.put(Instamojo.Environment.TEST, "https://test.instamojo.com/");
        env_options.put(Instamojo.Environment.PRODUCTION, "https://api.instamojo.com/");
    }
    private Instamojo.Environment mCurrentEnv = Instamojo.Environment.PRODUCTION;
    PaymentOptionDialog paymentOptionDialog;
    String transactionId;
    String instamojoOrderID;
    String instamojoTransactionID;
    String instamojoPaymentID;
    String instamojoPaymentStatus;
    int orderId;
    double codCharge;
    boolean prescriptionFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_list);
        getSupportActionBar().setTitle("Cart");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        setRadioBtns();
        setUpProgressDialog();
        setAddressBottomSheetDialog();
        setWalletCheckboxListener();
        initialisePresenter();
        mPresenter.fetchChargesList();
        mPresenter.getWalletDetails();
        setPaymentOptionDialog();
    }

    public void initialisePresenter() {
        mPresenter = new CartDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setRadioBtns() {
        radioBtnPrescription.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    radioBtnWithoutPrescription.setChecked(false);
                    txtViewWithoutPrescription.setVisibility(View.GONE);
                    layoutPrescription.setVisibility(View.VISIBLE);
                    prescriptionFlag = true;
                }
            }
        });

        radioBtnWithoutPrescription.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    radioBtnPrescription.setChecked(false);
                    txtViewWithoutPrescription.setVisibility(View.VISIBLE);
                    layoutPrescription.setVisibility(View.GONE);
                    prescriptionFlag = false;
                }
            }
        });

    }

    private void setPaymentOptionDialog() {
        paymentOptionDialog = new PaymentOptionDialog(this, this, this);
        paymentOptionDialog.setUpDialog();
    }

    private void setAddressBottomSheetDialog() {
        if (addressBottomSheetDialog == null) {
            View view = LayoutInflater.from(this).inflate(R.layout.layout_address_bottom_sheet, null);
            addressBottomSheetDialog = new BottomSheetDialog(this);
            txtViewAddAddress = (TextView) view.findViewById(R.id.txt_view_add_address);
            imgViewAddAddress = (ImageView) view.findViewById(R.id.img_view_add_address);
            recyclerViewShippingAddress = (RecyclerView) view.findViewById(R.id.recycler_view_address);
            txtViewAddAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), AddShippingAddressActivity.class);
                    startActivity(intent);
                }
            });
            addressBottomSheetDialog.setContentView(view);
        }
    }

    private void setWalletCheckboxListener() {
        walletCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (totalAmount >= walletAmount) {
                        double net = totalAmount - walletAmount;
                        txtViewTotal.setText("Rs. " + net);
                    } else {
                        txtViewTotal.setText("Rs. 0.0");
                    }
                } else {
                    txtViewTotal.setText("Rs. " + totalAmount);
                }
            }
        });
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private boolean checkAndRequestPermissions() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for(String perm: appPremisions){
            if(ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED){
                listPermissionsNeeded.add(perm);
            }
        }
        if(!listPermissionsNeeded.isEmpty()){
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void loadCartAdapter(CartListAdapter adapter, double totalAmount, double discount, double subTotal) {
        mainLayout.setVisibility(View.VISIBLE);
        recyclerViewCartList.setAdapter(adapter);
        recyclerViewCartList.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerViewCartList.addItemDecoration(itemDecor);

        txtViewSubTotal.setText("Rs. " + String.format("%.2f", subTotal));
        txtViewDiscount.setText("Rs. " + String.format("%.2f", discount));
        this.totalAmount = totalAmount;
        totalMrp = subTotal;
        this.discount = discount;

        for (int i = 0; i < this.charges.length; i++) {
            if (charges[i].type == 1 && checkChargeRange(charges[i].fromAmount - 1, charges[i].toAmount, totalAmount)) {
                deliveryCharges = charges[i].charge;
                break;
            }
        }
        this.totalAmount = this.totalAmount + deliveryCharges;
        txtViewDeliveryCharges.setText("Rs. " + deliveryCharges);
        if (walletCheckbox.isChecked()) {
            if (this.totalAmount > walletAmount) {
                double net = this.totalAmount - walletAmount;
                txtViewTotal.setText("Rs. " + String.format("%.2f", net));
            } else {
                txtViewTotal.setText( "Rs. 0.0");
            }
        } else {
            txtViewTotal.setText("Rs. " + String.format("%.2f", this.totalAmount));
        }

        paymentOptionDialog.setData(totalMrp, deliveryCharges, this.totalAmount, codCharge, discount);

    }

    @Override
    public void loadShippingAddressAdapter(CartShippingAddressAdapter cartShippingAddressAdapter) {
        recyclerViewShippingAddress.setAdapter(cartShippingAddressAdapter);
        recyclerViewShippingAddress.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerViewShippingAddress.addItemDecoration(itemDecor);
    }

    @Override
    public void onShippingAddressEditClicked(int addressId) {
        Intent intent = new Intent(this, EditAddressActivity.class);
        intent.putExtra("addressId", addressId);
        startActivity(intent);
    }

    @Override
    public void onAddressSelected(int addressId) {
        shippingAddressId = addressId;
    }

    @Override
    public void onDeliverButtonClicked() {
        if (prescriptionFlag && filePath == null) {
            Toasty.warning(this, "Please Upload a Prescription", Toasty.LENGTH_SHORT).show();
        } else {
            paymentOptionDialog.showDialog();
        }
    }

    @Override
    public void loadCharges(Charges[] charges) {
        this.charges = charges;
        View view = LayoutInflater.from(this).inflate(R.layout.layout_delivery_charges, null);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_delivery_charges);
        DeliveryChargesAdapter chargesAdapter = new DeliveryChargesAdapter(this, charges);
        recyclerView.setAdapter(chargesAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        balloon = new Balloon.Builder(this)
                                        .setLayout(view)
                                        .setBalloonAnimation(BalloonAnimation.OVERSHOOT)
                                        .setArrowOrientation(ArrowOrientation.BOTTOM)
                                        .setArrowColor(getResources().getColor(R.color.md_grey_700))
                                        .setMargin(10)
                                        .setAutoDismissDuration(5000)
                                        .build();
        mPresenter.fetchCartDetails();
        for (int i = 0; i < this.charges.length; i++) {
            if (this.charges[i].type == 2) {
                this.codCharge = this.charges[i].charge;
                break;
            }
        }
    }

    @Override
    public void loadWalletDetails(double walletAmount) {
        if (walletAmount < 1) {
            layoutWalletPay.setVisibility(View.GONE);
        } else {
            walletCheckbox.setText("Wallet Pay (Rs. " + walletAmount + ")");
            this.walletAmount = walletAmount;
        }
    }

    @Override
    public void showErrorMessage(String errorMsg) {

    }

    @Override
    public void hideViews() {
        mainLayout.setVisibility(View.GONE);
    }

    @Override
    public void hideExpressDeliveryOption() {
    }

    @Override
    public void changePrescriptionLayoutVisibility(boolean flag) {
        this.prescriptionFlag = flag;
        if (flag) {
            layoutPrescriptionMain.setVisibility(View.VISIBLE);
        } else {
            layoutPrescriptionMain.setVisibility(View.GONE);
        }
    }

    @Override
    public void loadPaymentData(OrderPlaceData orderPlaceData) {
        transactionId = UUID.randomUUID().toString();
        orderId = orderPlaceData.order.id;
        mPresenter.initiatePayment(orderPlaceData.paymentData.accessToken.accessToken, Double.toString(orderPlaceData.paymentData.amount), transactionId, mCurrentEnv.name());
    }

    @Override
    public void setOrderId(String id) {
        Instamojo.getInstance().initialize(this, mCurrentEnv);
        Instamojo.getInstance().initiatePayment(this, id,this);
    }

    @Override
    public void onPaymentVerifySuccess() {
        goToPaymentResponseActivity(1);
    }

    @Override
    public void onPaymentVerifyFail() {
        goToPaymentResponseActivity(2);
    }

    @Override
    public void goToCodPaymentResponse(String amount) {
        String orderMsg="";
        if (radioBtnPrescription.isChecked()  && prescriptionFlag) {
            orderMsg = "Order placed with “anymeds.in” and the prescription is being verified.";
        } else if (radioBtnWithoutPrescription.isChecked()) {
            orderMsg = "Order placed provisionally with anymeds.in. Our Doctor will call you to make a prescription for you.";
        } else {
            orderMsg = "Order placed successfully with anymeds.in and You will be updated once the order is packed and shipped.";
        }
        Intent intent = new Intent(this, PaymentResponseCodActivity.class);
        intent.putExtra("amount", amount);
        intent.putExtra("orderMsg", orderMsg);
        startActivity(intent);
        finish();
    }

    @Override
    public void showLoginBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }


    private void goToPaymentResponseActivity(int orderStatus) {
        String orderMsg="";
        if (radioBtnPrescription.isChecked() && prescriptionFlag) {
            orderMsg = "Order placed with “anymeds.in” and the prescription is being verified.";
        } else if (radioBtnWithoutPrescription.isChecked()) {
            orderMsg = "Order placed provisionally with anymeds.in. Our Doctor will call you to make a prescription for you.";
        } else {
            orderMsg = "Order placed successfully with anymeds.in and You will be updated once the order is packed and shipped.";
        }
        Intent intent = new Intent(this, PaymentResponseActivity.class);
        Log.e("LogMsg", "Order Id: "+ orderId);
        Log.e("LogMsg", "Order Status: "+ orderStatus);
        intent.putExtra("orderId", Integer.toString(orderId));
        intent.putExtra("paymentId", instamojoPaymentID);
        intent.putExtra("orderStatus", orderStatus);
        intent.putExtra("transactionId", instamojoTransactionID);
        intent.putExtra("orderMsg", orderMsg);
        intent.putExtra("amount", String.format("%.2f", totalAmount));
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.btn_select_delivery_address) void onSetDeliveryAddressClicked() {
        addressBottomSheetDialog.show();
    }

    @OnClick(R.id.layout_delivery_charges) void onDeliveryChargesClicked() {
        balloon.showAlignTop(imgViewExclamation);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchShippingAddressList();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean checkChargeRange(double fromAmount, double toAmount, double amount) {
        if (amount> fromAmount && amount < toAmount) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0) {
                loadImageChooser();
            } else {
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()) {
                    String permName = entry.getKey();
                    int permResult = entry.getValue();
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {
                        this.showAlertDialog("", "This app needs read and write storage permissions",
                                "Yes, Grant permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        checkAndRequestPermissions();
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required ", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                    } else {
                        this.showAlertDialog("", "You have denied some permissions",
                                "Go to settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                        break;
                    }

                }
            }
        }
    }

    public AlertDialog showAlertDialog(
            String title, String msg, String positiveLabel,
            DialogInterface.OnClickListener positiveOnClick,
            String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
            boolean isCancelable
    ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    @OnClick(R.id.layout_prescription) void onPrescriptionClicked() {
        if (checkAndRequestPermissions()) {
            loadImageChooser();
        }
    }

    @OnClick(R.id.btn_add_more) void onAddMoreClicked() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void loadImageChooser() {
        Intent galleryintent = new Intent(Intent.ACTION_PICK);
        galleryintent.setType("image/*");

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryintent);
        chooser.putExtra(Intent.EXTRA_TITLE, "Select from:");

        Intent[] intentArray = { cameraIntent };
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        startActivityForResult(chooser, REQUEST_PIC);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_PIC) {
            if (data.getData() != null) {
                filePath = getRealPathFromURI(data.getData(), this);
                if (calculateFileSize(filePath) > 2) {
                    Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                } else {
                    GlideHelper.setImageViewCustomRoundedCornersWithUri(this, imgViewPrescription, data.getData(), 150);
                }
            } else {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                filePath = saveImage(photo);
                if (calculateFileSize(filePath) > 2) {
                    Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                } else {
                    GlideHelper.setImageViewCustomRoundedCornersWithBitmap(this, imgViewPrescription, photo, 150);
                }
            }
        }
    }

    @Override
    public void onSubmitClicked(int paymentType) {
        paymentOptionDialog.hideDialog();
        int isWallet = 1;
        if (walletCheckbox.isChecked()) {
            isWallet = 2;
        }
        mPresenter.placeOrder(paymentType, shippingAddressId, 2, isWallet, filePath);    }

    @Override
    public void onInstamojoPaymentComplete(String orderID, String transactionID, String paymentID, String paymentStatus) {
        Log.e("LogMsg", "Order Id: " + orderID);
        Log.e("LogMsg", "transactionID: " + transactionID);
        Log.e("LogMsg", "paymentID: " + paymentID);
        Log.e("LogMsg", "paymentStatus: " + paymentStatus);
        this.instamojoOrderID = orderID;
        this.instamojoTransactionID = transactionID;
        this.instamojoPaymentID = paymentID;
        this.instamojoPaymentStatus = paymentStatus;
        mPresenter.verifyPayment(instamojoOrderID);
    }

    @Override
    public void onPaymentCancelled() {
        goToPaymentResponseActivity(2);
    }

    @Override
    public void onInitiatePaymentFailure(String s) {
        Log.e("LogMsg", "On Initiate Payment Failure: " + s);
        goToPaymentResponseActivity(2);
    }
}