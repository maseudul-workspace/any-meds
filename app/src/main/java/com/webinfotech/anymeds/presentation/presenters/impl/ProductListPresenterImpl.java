package com.webinfotech.anymeds.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.anymeds.AndroidApplication;
import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.AddToWishListInteractor;
import com.webinfotech.anymeds.domain.interactors.FetchProductListInteractor;
import com.webinfotech.anymeds.domain.interactors.FetchWishListInteractor;
import com.webinfotech.anymeds.domain.interactors.RemoveFromWishlistInteractor;
import com.webinfotech.anymeds.domain.interactors.impl.AddToWishListInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.FetchProductListInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.FetchWishListInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.RemoveFromWishlistInteractorImpl;
import com.webinfotech.anymeds.domain.models.Product;
import com.webinfotech.anymeds.domain.models.UserInfo;
import com.webinfotech.anymeds.domain.models.WishList;
import com.webinfotech.anymeds.presentation.presenters.ProductListPresenter;
import com.webinfotech.anymeds.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.anymeds.presentation.ui.adapters.ProductsVerticalAdapter;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;


public class ProductListPresenterImpl extends AbstractPresenter implements ProductListPresenter,
                                                                            FetchProductListInteractor.Callback,
                                                                            ProductsVerticalAdapter.Callback,
                                                                            AddToWishListInteractor.Callback,
                                                                            FetchWishListInteractor.Callback,
                                                                            RemoveFromWishlistInteractor.Callback
{

    Context mContext;
    ProductListPresenter.View mView;
    ProductsVerticalAdapter adapter;
    Product[] newProducts;
    int position;
    WishList[] wishLists;

    public ProductListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchProductList(int categoryId, int type, int page) {
        FetchProductListInteractorImpl fetchProductListInteractor = new FetchProductListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, categoryId, type, page);
        fetchProductListInteractor.execute();
    }

    @Override
    public void fetchWishList() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            mView.fetchProductList();
        } else {
            FetchWishListInteractorImpl fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchWishListInteractor.execute();
        }
    }

    @Override
    public void onGettingProductListSuccess(Product[] products, int totalPage) {
        if (wishLists != null) {
            for (int i = 0; i < wishLists.length; i++) {
                for (int j = 0; j < products.length; j++) {
                    if (products[j].id == wishLists[i].product.id) {
                        products[j].isWishListed = true;
                    }
                }
            }
        }
        Product[] tempProducts;
        tempProducts = newProducts;
        try {
            int len1 = tempProducts.length;
            int len2 = products.length;
            newProducts = new Product[len1 + len2];
            System.arraycopy(tempProducts, 0, newProducts, 0, len1);
            System.arraycopy(products, 0, newProducts, len1, len2);
            adapter.updateDataSet(newProducts);
            adapter.notifyDataSetChanged();
        }catch (NullPointerException e){
            newProducts = products;
            adapter = new ProductsVerticalAdapter(mContext, products, this);
            mView.loadAdapter(adapter, totalPage);
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingProductListFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onProductClicked(int productId) {
        mView.onProductClicked(productId);
    }

    @Override
    public void addToWishList(int productId, int position) {
        this.position = position;
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            mView.showLoginBottomSheet();
        } else {
            AddToWishListInteractorImpl addToWishListInteractor = new AddToWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId, productId);
            addToWishListInteractor.execute();
        }
    }

    @Override
    public void removeFromWishList(int productId, int position) {
        this.position = position;
        int wishListId = 0;
        for (int i = 0; i < wishLists.length; i++) {
            if (wishLists[i].product.id == productId) {
                wishListId = wishLists[i].wishListId;
                break;
            }
        }
        this.position = position;
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            mView.showLoginBottomSheet();
        } else {
            RemoveFromWishlistInteractorImpl removeFromWishlistInteractor = new RemoveFromWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, wishListId);
            removeFromWishlistInteractor.execute();
        }
    }

    @Override
    public void onAddToWishListSuccess() {
        Toasty.normal(mContext, "Added To Wish List").show();
        adapter.onAddWishListSuccess(position);
    }

    @Override
    public void onAddToWishListFail(String errorMsg, int loginError) {

    }

    @Override
    public void onWishListFetchSuccess(WishList[] wishLists) {
        this.wishLists = wishLists;
        mView.fetchProductList();
    }

    @Override
    public void onWishListFetchFail(String errorMsg, int loginError) {
        mView.fetchProductList();
    }

    @Override
    public void onWishListRemoveSuccess() {
        Toasty.normal(mContext, "Removed From Wishlist").show();
        adapter.onWishListRemoveSuccess(position);
    }

    @Override
    public void onWishListRemoveFail(String errorMsg, int loginError) {

    }
}
