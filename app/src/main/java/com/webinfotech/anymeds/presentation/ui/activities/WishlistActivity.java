package com.webinfotech.anymeds.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.anymeds.R;
import com.webinfotech.anymeds.domain.executors.impl.ThreadExecutor;
import com.webinfotech.anymeds.presentation.presenters.WishListPresenter;
import com.webinfotech.anymeds.presentation.presenters.impl.WishListPresenterImpl;
import com.webinfotech.anymeds.presentation.ui.adapters.WishlistAdapter;
import com.webinfotech.anymeds.threading.MainThreadImpl;

public class WishlistActivity extends AppCompatActivity implements WishListPresenter.View {

    @BindView(R.id.recycler_view_wishlist)
    RecyclerView recyclerViewWishList;
    WishListPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);
        getSupportActionBar().setTitle("Wishlist");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        showLoader();
        mPresenter.fetchWishList();
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void initialisePresenter() {
        mPresenter = new WishListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadAdapter(WishlistAdapter adapter) {
        recyclerViewWishList.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerViewWishList.setAdapter(adapter);
    }

    @Override
    public void onProductClicked(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}