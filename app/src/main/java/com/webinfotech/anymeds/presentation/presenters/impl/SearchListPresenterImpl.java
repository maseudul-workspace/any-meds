package com.webinfotech.anymeds.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.FetchSearchListInteractor;
import com.webinfotech.anymeds.domain.interactors.impl.FetchSearchListInteractorImpl;
import com.webinfotech.anymeds.domain.models.Product;
import com.webinfotech.anymeds.presentation.presenters.SearchListPresenter;
import com.webinfotech.anymeds.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.anymeds.presentation.ui.adapters.SearchListAdapter;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

public class SearchListPresenterImpl extends AbstractPresenter implements   SearchListPresenter,
                                                                            FetchSearchListInteractor.Callback,
                                                                            SearchListAdapter.Callback
{

    Context mContext;
    SearchListPresenter.View mView;
    String searchKey;

    public SearchListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchSearchList(String searchKey) {
        this.searchKey = searchKey;
        FetchSearchListInteractorImpl fetchSearchListInteractor = new FetchSearchListInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), searchKey);
        fetchSearchListInteractor.execute();
    }

    @Override
    public void onGettingSearchListSuccess(Product[] products, String searchKey) {
        if (this.searchKey.equals(searchKey)) {
            if (products.length > 0) {
                SearchListAdapter adapter = new SearchListAdapter(mContext, products, this);
                mView.loadAdapter(adapter);
            } else {
                mView.onSearchNotFound(searchKey);
            }
        }
    }

    @Override
    public void onGettingSearchListFail(String errorMsg) {

    }

    @Override
    public void onProductClicked(int productId) {
        mView.onProductClicked(productId);
    }
}
