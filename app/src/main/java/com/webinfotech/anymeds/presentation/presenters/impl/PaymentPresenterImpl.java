package com.webinfotech.anymeds.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.anymeds.AndroidApplication;
import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.PlaceOrderInteractor;
import com.webinfotech.anymeds.domain.interactors.impl.PlaceOrderInteractorImpl;
import com.webinfotech.anymeds.domain.models.OrderPlaceData;
import com.webinfotech.anymeds.domain.models.UserInfo;
import com.webinfotech.anymeds.presentation.presenters.PaymentPresenter;
import com.webinfotech.anymeds.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class PaymentPresenterImpl extends AbstractPresenter implements PaymentPresenter, PlaceOrderInteractor.Callback {

    Context mContext;
    PaymentPresenter.View mView;

    public PaymentPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void placeOrder(int paymentMethod, int addressId, int orderType, int isWallet, String prescription) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            PlaceOrderInteractorImpl placeOrderInteractor = new PlaceOrderInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, paymentMethod, addressId, orderType, isWallet, 0, "", prescription);
            placeOrderInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onPlaceOrderSuccess(OrderPlaceData orderPlaceData) {
        mView.loadPaymentData(orderPlaceData);
        mView.hideLoader();
    }

    @Override
    public void onPlaceOrderFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }
}
