package com.webinfotech.anymeds.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.anymeds.AndroidApplication;
import com.webinfotech.anymeds.R;

public class UserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("User Activity");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.layout_shipping_address) void onShippingAddressClicked() {
        Intent intent = new Intent(this, ShippingAddressActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_user_profile) void onUserProfileClicked() {
        Intent intent = new Intent(this, UserProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_change_password) void onChangePasswordClicked() {
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_wishlist) void onWishlistClicked() {
        Intent intent = new Intent(this, WishlistActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_wallet) void onWalletClicked() {
        Intent intent = new Intent(this, WalletHistoryActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_orders) void onOrdersClicked() {
        Intent intent = new Intent(this, OrderHistoryActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_log_out) void onLogoutClicked() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setUserInfo(this, null);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}