package com.webinfotech.anymeds.presentation.presenters;

import com.webinfotech.anymeds.domain.models.Product;
import com.webinfotech.anymeds.presentation.ui.adapters.RelatedProductsAdapter;

public interface ProductDetailsPresenter {
    void fetchProductDetails(int productId);
    void addToCart(int productId, String qty, String sizeId);
    void fetchCartDetails();
    void checkPin(String pin);
    interface View {
        void loadData(Product product, RelatedProductsAdapter relatedProductsAdapter);
        void loadCartCount(int cartCount);
        void onProductClicked(int productId);
        void showLoginSnackbar();
        void showLoader();
        void hideLoader();
        void showCartSnackbar();
        void setPinMessage(String message);
    }
}
