package com.webinfotech.anymeds.presentation.presenters;

public interface RegisterUserPresenter {
    void registerUser(String name,
                      String email,
                      String mobile,
                      String state,
                      String city,
                      String pin,
                      String address,
                      String password,
                      String confirmPassword);
    interface View {
        void onRegisterSuccess();
        void showLoader();
        void hideLoader();
    }
}
