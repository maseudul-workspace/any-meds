package com.webinfotech.anymeds.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.instamojo.android.models.Wallet;
import com.webinfotech.anymeds.AndroidApplication;
import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.GetWalletDetailsInteractor;
import com.webinfotech.anymeds.domain.interactors.impl.GetWalletDetailsInteractorImpl;
import com.webinfotech.anymeds.domain.models.UserInfo;
import com.webinfotech.anymeds.domain.models.WalletDetails;
import com.webinfotech.anymeds.presentation.presenters.WalletHistoryPresenter;
import com.webinfotech.anymeds.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.anymeds.presentation.ui.adapters.WalletHistoryAdapter;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class WalletHistoryPresenterImpl extends AbstractPresenter implements WalletHistoryPresenter, GetWalletDetailsInteractorImpl.Callback {

    Context mContext;
    WalletHistoryPresenterImpl.View mView;
    WalletHistoryAdapter adapter;

    public WalletHistoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchWalletHistory() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            GetWalletDetailsInteractorImpl getWalletDetailsInteractor = new GetWalletDetailsInteractorImpl(mExecutor, mMainThread,  new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            getWalletDetailsInteractor.execute();
            mView.showLoader();
        } else {
            Toasty.error(mContext, "Your Session Expired!!! Please Login Again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onGettingCreditHistorySuccess(WalletDetails walletDetails) {
        WalletHistoryAdapter adapter = new WalletHistoryAdapter(mContext, walletDetails.walletHistories);
        mView.loadAdapter(adapter, walletDetails);
        mView.hideLoader();
    }

    @Override
    public void onGettingCreditHistoryFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            Toasty.warning(mContext, "Session expired !! Please Login Again").show();
            mView.showLoginBottomSheet();
        } else {
            Toasty.warning(mContext, errorMsg).show();
        }
    }
}
