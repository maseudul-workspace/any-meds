package com.webinfotech.anymeds.presentation.presenters;


import com.webinfotech.anymeds.presentation.ui.adapters.SubcategoryAdapter;

public interface SubcategoryPresenter {
    void fetchSubcategories(int categoryId);
    interface View {
        void loadData(SubcategoryAdapter adapter);
        void showLoader();
        void hideLoader();
        void onSubcategoryClicked(int id);
    }
}
