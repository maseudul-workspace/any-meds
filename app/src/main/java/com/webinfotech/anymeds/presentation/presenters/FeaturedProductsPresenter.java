package com.webinfotech.anymeds.presentation.presenters;

import com.webinfotech.anymeds.presentation.ui.adapters.ProductsVerticalAdapter;

public interface FeaturedProductsPresenter {
    void fetchFeaturedProducts();
    void fetchWishList();
    interface View {
        void loadAdapter(ProductsVerticalAdapter adapter);
        void showLoader();
        void hideLoader();
        void showLoginBottomSheet();
        void goToProductDetails(int id);
    }
}
