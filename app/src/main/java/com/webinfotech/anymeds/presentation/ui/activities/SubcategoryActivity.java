package com.webinfotech.anymeds.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.anymeds.R;
import com.webinfotech.anymeds.domain.executors.impl.ThreadExecutor;
import com.webinfotech.anymeds.presentation.presenters.SubcategoryPresenter;
import com.webinfotech.anymeds.presentation.presenters.impl.SubcategoryPresenterImpl;
import com.webinfotech.anymeds.presentation.ui.adapters.SubcategoryAdapter;
import com.webinfotech.anymeds.threading.MainThreadImpl;

public class SubcategoryActivity extends AppCompatActivity implements SubcategoryPresenter.View {

    @BindView(R.id.recycler_view_subcategory)
    RecyclerView recyclerViewSubcategory;
    SubcategoryPresenterImpl mPresenter;
    int categoryId;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcategory);
        ButterKnife.bind(this);
        categoryId = getIntent().getIntExtra("categoryId", 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Health Products");
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchSubcategories(categoryId);
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new SubcategoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadData(SubcategoryAdapter adapter) {
        recyclerViewSubcategory.setAdapter(adapter);
        recyclerViewSubcategory.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onSubcategoryClicked(int subcategoryId) {
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra("categoryId", subcategoryId);
        intent.putExtra("type", 2);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}