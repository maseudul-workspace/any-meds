package com.webinfotech.anymeds.presentation.presenters;

import com.webinfotech.anymeds.domain.models.HomeData;

public interface MainPresenter {
    void fetchHomeData();
    void fetchCartDetails();
    interface View {
        void onGettingHomeDataSuccess(HomeData homeData);
        void loadCartCount(int cartCount);
        void showLoader();
        void hideLoader();
    }
}
