package com.webinfotech.anymeds.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.webinfotech.anymeds.AndroidApplication;
import com.webinfotech.anymeds.R;
import com.webinfotech.anymeds.domain.models.UserInfo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PaymentResponseCodActivity extends AppCompatActivity {

    @BindView(R.id.txt_view_order_date)
    TextView txtViewOrderDate;
    @BindView(R.id.txt_view_amount)
    TextView txtViewAmount;
    @BindView(R.id.txt_view_name)
    TextView txtViewName;
    @BindView(R.id.txt_view_order_msg)
    TextView txtViewOrderMsg;
    String amount;
    String orderMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_response_cod);
        amount = getIntent().getStringExtra("amount");
        orderMsg = getIntent().getStringExtra("orderMsg");
        ButterKnife.bind(this);
        setData();
    }

    private void setData() {
        txtViewOrderMsg.setText(orderMsg);
        txtViewAmount.setText("Rs. " + amount);
        txtViewOrderDate.setText(getDate());
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        txtViewName.setText(userInfo.name);
    }

    public String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "MMMM dd, yyyy", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    @OnClick(R.id.btn_order_history) void onOrderHistoryClicked() {
        Intent intent = new Intent(this, OrderHistoryActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_home_page) void onHomePageClicked() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}