package com.webinfotech.anymeds.presentation.presenters;

import com.webinfotech.anymeds.presentation.ui.adapters.ShippingAddressAdapter;

public interface ShippingAddressPresenter {
    void fetchShippingAddress();
    interface View {
        void loadAdapter(ShippingAddressAdapter adapter);
        void goToAddressDetails(int addressId);
        void onAddressFetchFailed();
        void showLoader();
        void hideLoader();
        void showLoginBottomSheet();
    }
}
