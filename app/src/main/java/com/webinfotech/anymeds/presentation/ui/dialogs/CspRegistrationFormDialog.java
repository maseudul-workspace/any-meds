package com.webinfotech.anymeds.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.util.Patterns;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.webinfotech.anymeds.R;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class CspRegistrationFormDialog {

    public interface Callback {
        void submitCspForm(String applicantName, String pharmacyName, String address, String pin, String phoneNo, String email, String phonePe, String bankAC, String ifsc);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_pharmacy)
    EditText edit_text_pharmacy;
    @BindView(R.id.edit_text_address)
    EditText edit_text_address;
    @BindView(R.id.edit_text_phone_no)
    EditText edit_text_phone_no;
    @BindView(R.id.edit_text_phone_pe)
    EditText edit_text_phone_pe;
    @BindView(R.id.edit_text_bank_ac)
    EditText edit_text_bank_ac;
    @BindView(R.id.edit_text_ifsc)
    EditText edit_text_ifsc;
    @BindView(R.id.edit_text_email)
    EditText edit_text_email;
    @BindView(R.id.edit_text_pin)
    EditText edit_text_pin;
    @BindView(R.id.check_box_agreement)
    CheckBox checkBoxAgreement;

    public CspRegistrationFormDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_csp_registration_form, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    @OnClick(R.id.img_view_cancel) void onCancelClicked() {
        hideDialog();
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (    editTextName.getText().toString().trim().isEmpty() ||
                edit_text_pharmacy.getText().toString().trim().isEmpty() ||
                edit_text_address.getText().toString().trim().isEmpty() ||
                edit_text_phone_no.getText().toString().trim().isEmpty()
        ) {
            Toasty.warning(mContext, "Some required fields are empty", Toasty.LENGTH_SHORT).show();
        } else if (edit_text_phone_no.getText().toString().trim().length() < 10) {
            Toasty.warning(mContext, "Please enter a 10 digit phone no", Toasty.LENGTH_SHORT).show();
        } else if (!checkBoxAgreement.isChecked()) {
            Toasty.warning(mContext, "Please check the terms and condition", Toasty.LENGTH_SHORT).show();
        } else {
            mCallback.submitCspForm(    editTextName.getText().toString(),
                                        edit_text_pharmacy.getText().toString(),
                                        edit_text_address.getText().toString(),
                                        edit_text_pin.getText().toString(),
                                        edit_text_phone_no.getText().toString(),
                                        edit_text_email.getText().toString(),
                                        edit_text_phone_pe.getText().toString(),
                                        edit_text_bank_ac.getText().toString(),
                                        edit_text_ifsc.getText().toString()
                    );
        }
    }

}
