package com.webinfotech.anymeds.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.webinfotech.anymeds.AndroidApplication;
import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.FetchUserProfileInteractor;
import com.webinfotech.anymeds.domain.interactors.UpdateUserInteractor;
import com.webinfotech.anymeds.domain.interactors.impl.FetchUserProfileInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.UpdateUserInteractorImpl;
import com.webinfotech.anymeds.domain.models.UserInfo;
import com.webinfotech.anymeds.presentation.presenters.UserProfilePresenter;
import com.webinfotech.anymeds.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class UserProfilePresenterImpl extends AbstractPresenter implements UserProfilePresenter,
                                                                            FetchUserProfileInteractor.Callback,
                                                                            UpdateUserInteractor.Callback

{

    Context mContext;
    UserProfilePresenter.View mView;
    AndroidApplication androidApplication;
    FetchUserProfileInteractorImpl fetchUserProfileInteractor;
    UpdateUserInteractorImpl updateUserInteractor;

    public UserProfilePresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchUserProfile() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            fetchUserProfileInteractor = new FetchUserProfileInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.userId, user.apiToken);
            fetchUserProfileInteractor.execute();
            mView.showLoader();
        }

    }

    @Override
    public void updateProfile(String name, String email, String mobile, String city, String state, String pin, String address) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            updateUserInteractor = new UpdateUserInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.userId, user.apiToken, name, email, mobile, state, city, pin, address);
            updateUserInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingUserProfileSuccess(UserInfo userInfo) {
        mView.hideLoader();
        mView.loadUserProfileData(userInfo);
    }

    @Override
    public void onGettingUserProfileFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onUserUpdateSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "User Updated Successfully").show();
    }

    @Override
    public void onUserUpdateFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            Toasty.warning(mContext, "Session expired !! Please Login Again").show();
            mView.showLoginBottomSheet();
        } else {
            Toasty.warning(mContext, errorMsg).show();
        }
    }
}
