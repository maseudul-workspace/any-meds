package com.webinfotech.anymeds.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.anymeds.R;
import com.webinfotech.anymeds.domain.models.Charges;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DeliveryChargesAdapter extends RecyclerView.Adapter<DeliveryChargesAdapter.ViewHolder> {

    Context mContext;
    Charges[] charges;

    public DeliveryChargesAdapter(Context mContext, Charges[] charges) {
        this.mContext = mContext;
        this.charges = charges;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_charges_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewCharges.setText("* " + charges[position].description);
    }

    @Override
    public int getItemCount() {
        return charges.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_charges)
        TextView txtViewCharges;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
