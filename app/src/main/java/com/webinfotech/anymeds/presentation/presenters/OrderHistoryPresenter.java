package com.webinfotech.anymeds.presentation.presenters;


import com.webinfotech.anymeds.domain.models.OrderPlaceData;
import com.webinfotech.anymeds.presentation.ui.adapters.OrdersAdapter;

public interface OrderHistoryPresenter {
    void fetchOrders(int pageNo, String refresh);
    void initiatePayment(String accessToken, String amount, String transactionId, String env);
    void requestPayment(String accessToken, String id);
    void setTransactionId(int orderId, String transactionId);
    void verifyPayment(String instamojoOrderId);
    interface View {
        void loadAdapter(OrdersAdapter adapter, int totalPage);
        void goToProductDetails(int productId, int productType);
        void showPrescription(String prescription);
        void showLoader();
        void hideLoader();
        void cancelOrder();
        void loadPaymentData(OrderPlaceData orderPlaceData);
        void setOrderId(String id);
        void onPaymentVerifySuccess();
        void onPaymentVerifyFail();
        void showLoginBottomSheet();
    }
}
