package com.webinfotech.anymeds.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;

import com.webinfotech.anymeds.R;
import com.webinfotech.anymeds.util.GlideHelper;

public class LabDiagnosticsActivity extends AppCompatActivity {

    @BindView(R.id.img_view_banner)
    ImageView imgViewBanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lab_diagnostics);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Labs and Diagnostics");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        GlideHelper.setImageView(this, imgViewBanner, "https://anymeds.in/web/images/lab/1.jpg");
    }

    @OnClick(R.id.btn_find_top_diagnostics) void onFindClicked() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://doctor.anymeds.in/"));
        startActivity(browserIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}