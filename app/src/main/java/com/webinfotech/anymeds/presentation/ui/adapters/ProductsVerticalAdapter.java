package com.webinfotech.anymeds.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.anymeds.R;
import com.webinfotech.anymeds.domain.models.Product;
import com.webinfotech.anymeds.util.GlideHelper;

import org.w3c.dom.Text;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductsVerticalAdapter extends RecyclerView.Adapter<ProductsVerticalAdapter.ViewHolder> {

    public interface Callback {
        void onProductClicked(int productId);
        void addToWishList(int productId, int position);
        void removeFromWishList(int productId, int position);
    }

    Context mContext;
    Product[] products;
    Callback mCallback;

    public ProductsVerticalAdapter(Context mContext, Product[] products, Callback mCallback) {
        this.mContext = mContext;
        this.products = products;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_products_vertical, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageView(mContext, holder.imgViewProduct, mContext.getResources().getString(R.string.base_url) + "/images/products/thumb/" + products[position].mainImage);
        holder.txtViewProductName.setText(products[position].name);
        holder.txtViewPrice.setText("₹ " + products[position].minPrice);
        holder.txtViewMrp.setText("₹ " + products[position].mrp);
        holder.txtViewMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        double discount = ((products[position].mrp - products[position].minPrice)/products[position].mrp)*100;
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(products[position].id);
            }
        });

        holder.txtViewDiscount.setText(String.format("%.1f", discount) + "% Discount");

    }

    @Override
    public int getItemCount() {
        return products.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.txt_view_discount)
        TextView txtViewDiscount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(Product[] products) {
        this.products = products;
        notifyDataSetChanged();
    }

    public void onAddWishListSuccess(int position) {
        this.products[position].isWishListed = true;
        notifyItemChanged(position);
    }

    public void onWishListRemoveSuccess(int position) {
        this.products[position].isWishListed = false;
        notifyItemChanged(position);
    }

}
