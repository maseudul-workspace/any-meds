package com.webinfotech.anymeds.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.webinfotech.anymeds.AndroidApplication;
import com.webinfotech.anymeds.R;
import com.webinfotech.anymeds.domain.executors.impl.ThreadExecutor;
import com.webinfotech.anymeds.domain.models.Doctors;
import com.webinfotech.anymeds.domain.models.HomeData;
import com.webinfotech.anymeds.domain.models.Labs;
import com.webinfotech.anymeds.domain.models.UserInfo;
import com.webinfotech.anymeds.domain.models.testing.Image;
import com.webinfotech.anymeds.domain.models.testing.Item;
import com.webinfotech.anymeds.presentation.presenters.MainPresenter;
import com.webinfotech.anymeds.presentation.presenters.PharmacyListPresenter;
import com.webinfotech.anymeds.presentation.presenters.impl.MainPresenterImpl;
import com.webinfotech.anymeds.presentation.ui.adapters.BannersViewpagerAdapter;
import com.webinfotech.anymeds.presentation.ui.adapters.DoctorsAppointmentAdapter;
import com.webinfotech.anymeds.presentation.ui.adapters.HorizontalItemsAdapter;
import com.webinfotech.anymeds.presentation.ui.adapters.LabDiagnosticsAdapter;
import com.webinfotech.anymeds.presentation.ui.adapters.MainViewPagerAdapter;
import com.webinfotech.anymeds.presentation.ui.adapters.TopPharmaciesAdapter;
import com.webinfotech.anymeds.presentation.ui.adapters.VerticalItemsAdapter;
import com.webinfotech.anymeds.presentation.ui.bottomsheets.LoginBottomSheet;
import com.webinfotech.anymeds.threading.MainThreadImpl;
import com.webinfotech.anymeds.util.GlideHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends BaseActivity implements   DoctorsAppointmentAdapter.Callback,
                                                            LabDiagnosticsAdapter.Callback,
                                                            TopPharmaciesAdapter.Callback,
                                                            MainPresenter.View,
                                                            VerticalItemsAdapter.Callback,
                                                            HorizontalItemsAdapter.Callback
{

    @BindView(R.id.viewpager1)
    ViewPager viewPager1;
    @BindView(R.id.viewpager2)
    ViewPager viewPager2;
    @BindView(R.id.viewpager3)
    ViewPager viewPager3;
    @BindView(R.id.recycler_view_items)
    RecyclerView recyclerViewItems;
    @BindView(R.id.recycler_view_doctors)
    RecyclerView recyclerViewDoctors;
    @BindView(R.id.recycler_view_lab_diagnostics)
    RecyclerView recyclerViewLabDiagnostics;
    @BindView(R.id.recycler_view_top_pharmacies)
    RecyclerView recyclerViewTopPharmacies;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.recycler_view_popular_products)
    RecyclerView recyclerViewPopularProducts;
    MainPresenterImpl mPresenter;
    String[] appPremisions = {
            Manifest.permission.CALL_PHONE,
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;
    private int currentPage = 0;
    private int currentPage1 = 0;
    private int currentPage2 = 0;
    @BindView(R.id.txt_view_cart_count)
    TextView txtViewCartCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        ButterKnife.bind(this);
        bottomNavigationView.setItemIconTintList(null);
        setData();
        setBottomNavigationView();
        initialisePresenter();
        mPresenter.fetchHomeData();
    }

    private void initialisePresenter() {
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setBottomNavigationView() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_my_account:
                       if (isLoggedIn()) {
                           goToUserActivity();
                       } else {
                           showLoginBottomSheet();
                       }
                       break;
                    case R.id.nav_order_medicine:
                        goToOrderMedicineActivity();
                        break;
                    case R.id.nav_my_doctor:
                        goToMyDoctorActivity();
                        break;
                    case R.id.nav_order_whatsapp:
                        goToOrderThroughWhatsappActivity();
                        break;
                }
                return false;
            }
        });
    }

    private boolean isLoggedIn() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(getApplicationContext());
        if (userInfo == null) {
            return false;
        } else {
            Log.e("LogMsg", "User Id: " + userInfo.userId);
            Log.e("LogMsg", "Api token: " + userInfo.apiToken);
            return true;
        }
    }

    private void showLoginBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

    private void setData() {

        setRecyclerViewDoctors();
        setRecyclerViewLabDiagnostics();
    }

    private void setRecyclerViewDoctors() {

        ArrayList<Doctors> doctors = new ArrayList<>();
        Doctors doctors1 = new Doctors(1, "Dr.Amlan Jyoti Sarmah", "MBBS,MS,DNB", "Urologist,Genito Urinary surgeon", "Hayat Hospital,Guwahati", getResources().getString(R.string.base_url) + "web/images/doctor/all_doctor/1.jpg");
        Doctors doctors2 = new Doctors(1, "Dr.Deepanova Jyoti Das", "MBBS,MS(ENT)", "Consultant ENT", "Hayat Hospital,Guwahati", getResources().getString(R.string.base_url) + "web/images/doctor/all_doctor/2.jpg");
        Doctors doctors3 = new Doctors(1, "Dr.A M Khandakar", "MBBS,MD", "Paediatrician", "Dispur Polyclinic & Nursing home,Guwahati", getResources().getString(R.string.base_url) + "web/images/doctor/all_doctor/3.jpg");
        Doctors doctors4 = new Doctors(1, "Dr.Bhaskar Neog", "MBBS,DGM,F.Diab", "Geriatric medicine & Diabetology", "Hayat Hospital,Guwahati", getResources().getString(R.string.base_url) + "web/images/doctor/all_doctor/4.jpg");
        Doctors doctors5 = new Doctors(1, "Dr.Sobur Uddin Ahmed", "MBBS,DM", "Gastroenterologist", "Hayat Hospital,Guwahati", getResources().getString(R.string.base_url) + "web/images/doctor/all_doctor/5.jpg");
        Doctors doctors6 = new Doctors(1, "Dr.Pankaj Kr.Bhagwati", "MBBS-MS,FMAS,FIAGES", "General & Laparoscopic Surgeon", "Hayat Hospital,Guwahati", getResources().getString(R.string.base_url) + "web/images/doctor/all_doctor/6.jpg");
        Doctors doctors7 = new Doctors(1, "Dr.Anup Baishya", "BAMS,MD(Ayur)", "Ayurvedic medicine", "Mausum Medicos", getResources().getString(R.string.base_url) + "web/images/doctor/all_doctor/7.jpg");
        Doctors doctors8 = new Doctors(1, "Dr.Jutimala Bordoloi Bhattacharya", "MBBS,MS(O&G)", "Obs&Gynaecologist,Infertility Specialist,Laparoscopic surgeon(O&G)", "Dispur Polyclinic & Nursing home,Guwahati", getResources().getString(R.string.base_url) + "web/images/doctor/all_doctor/8.jpg");
        Doctors doctors9 = new Doctors(1, "Dr.Twinkle Daulagupahu", "MBBS,MD", "Dermatologist", "Dispur Polyclinic & Nursing home,Guwahati", getResources().getString(R.string.base_url) + "web/images/doctor/all_doctor/9.jpg");
        Doctors doctors10 = new Doctors(1, "Dr. Satish Bawri", "MBBS,MD,DM", "Neurologist", "Dispur poly clinic,Apollo Hospital", getResources().getString(R.string.base_url) + "web/images/doctor/all_doctor/10.jpg");
        Doctors doctors11 = new Doctors(1, "Dr. Devanga Sarma", "MBBS,MS,MCH(urology)", "Consultant Urologist", "Gate Hospital,Guwahati", getResources().getString(R.string.base_url) + "web/images/doctor/all_doctor/11.jpg");
        Doctors doctors12 = new Doctors(1, "Dr.Samim Ahmed", "MBBS,MS(Ortho)", "Consultant Orthopaedic Surgeon", "Night angle hospital,Silchar", getResources().getString(R.string.base_url) + "web/images/doctor/all_doctor/12.jpg");

        doctors.add(doctors1);
        doctors.add(doctors2);
        doctors.add(doctors3);
        doctors.add(doctors4);
        doctors.add(doctors5);
        doctors.add(doctors6);
        doctors.add(doctors7);
        doctors.add(doctors8);
        doctors.add(doctors9);
        doctors.add(doctors10);
        doctors.add(doctors11);
        doctors.add(doctors12);

        DoctorsAppointmentAdapter doctorsAppointmentAdapter = new DoctorsAppointmentAdapter(this, doctors, this);
        recyclerViewDoctors.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewDoctors.setAdapter(doctorsAppointmentAdapter);
    }

    private void setRecyclerViewLabDiagnostics() {
        ArrayList<Labs> labs = new ArrayList<>();

        Labs labs1 = new Labs(1, "Life Care Diagnostics", "Maligaon", "Blood & Other Routine tests,Microbiological tests,Pulmonary function test,Endoscopy ,Sputum for AFB,ECG etc", getResources().getString(R.string.base_url) + "web/images/lab/all_lab/1.jpg");
        Labs labs2 = new Labs(2, "Ayursundra diagnostics", "Maligaon", "Blood & Otherb Routine tests,Microbiological tests,Cardiac CT scan,High resolut ultra sound,16 slice CT scan(multi detect), Digital x ray etc", getResources().getString(R.string.base_url) + "web/images/lab/all_lab/2.jpg");
        Labs labs3 = new Labs(3, "Primus", "Bhangagarh", "Blood & Other Routine tests,Microbiological tests,MRI,CT scan,X-Ray,SSEP,BMD,Echo,TMT etc", getResources().getString(R.string.base_url) + "web/images/lab/all_lab/3.jpg");
        Labs labs4 = new Labs(4, "Matrix", "Christian Basti", "Blood & Other Routine tests, Microbiological tests, MRI,Doppler, CT scan, COVID antibody test, Serum beta HCG etc", getResources().getString(R.string.base_url) + "web/images/lab/all_lab/4.jpg");
        Labs labs5 = new Labs(5, "Alcare diagnostic & research centre pvt.ltd", "Rajgarh", "Blood & Other Routine tests, Microbiological tests, PCR, Endoscopy, Colonoscopy, Dental OPG, Histo & Cytopathology, CT scan etc", getResources().getString(R.string.base_url) + "web/images/lab/all_lab/5.jpg");
        Labs labs6 = new Labs(6, "Ultra care diagnostic centre", "Beltola", "Blood & Other Routine tests,Micropbiological tests,COVID-19 RTPCR,Cyto&Histopathology, CSF Culture,Fungal Culture,CT scan,ECG,Dopplytest Etc", getResources().getString(R.string.base_url) + "web/images/lab/all_lab/6.jpg");
        Labs labs7 = new Labs(7, "Sky lab SRL diagnostic", "Ulubari", "Blood & Other Routine tests, Microbiological Tests, Comprehensive Health Check, USG, X-ray, Viral Profile etc", getResources().getString(R.string.base_url) + "web/images/lab/all_lab/7.jpg");
        Labs labs8 = new Labs(8, "Dr. Lal Path Labs", "Ganeshguri", "Blood & Other Routine tests,Microbiological Tests,SARS2 COVID-19 antibody test,CRP,TFT,Lipid profile,vitamin D,ECG Etc", getResources().getString(R.string.base_url) + "web/images/lab/all_lab/8.jpg");
        Labs labs9 = new Labs(9, "Bioscan Diagnostics", "Adabari", "Blood & Other Routine tests,Microbiological tests,Pulmonary function test,Endoscopy ,Sputum for AFB,ECG etc", getResources().getString(R.string.base_url) + "web/images/lab/all_lab/9.jpg");
        Labs labs10 = new Labs(10, "Thyrocare", "Ulubari", "Blood & Other Routine tests, Microbiological tests etc", getResources().getString(R.string.base_url) + "web/images/lab/all_lab/10.jpg");

        labs.add(labs1);
        labs.add(labs2);
        labs.add(labs3);
        labs.add(labs4);
        labs.add(labs5);
        labs.add(labs6);
        labs.add(labs7);
        labs.add(labs8);
        labs.add(labs9);
        labs.add(labs10);

        LabDiagnosticsAdapter labDiagnosticsAdapter = new LabDiagnosticsAdapter(this, labs, this);
        recyclerViewLabDiagnostics.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewLabDiagnostics.setAdapter(labDiagnosticsAdapter);
    }

    private void goToOrderMedicineActivity() {
        Intent intent = new Intent(this, OrderMedicineActivity.class);
        startActivity(intent);
    }

    private void goToMyDoctorActivity() {
        Intent intent = new Intent(this, MyDoctorsActivity.class);
        startActivity(intent);
    }

    private void goToOrderThroughWhatsappActivity() {
        Intent intent = new Intent(this, OrderThroughWhatsappActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_doctors_view_more) void onSearchDoctorClicked() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://doctor.anymeds.in/"));
        startActivity(browserIntent);
    }

    @OnClick(R.id.btn_labs_view_more) void onSearchLabsClicked() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://doctor.anymeds.in/"));
        startActivity(browserIntent);
    }

    @Override
    public void onBookNowClicked() {
        dialNumber();
    }

    @Override
    public void onBookAppointmentClicked() {
        dialNumber();
    }

    @OnClick(R.id.btn_ask_doctor) void onAskDoctorClicked() {
        dialNumber();
    }

    @OnClick(R.id.btn_call_medicine) void onCallMedicineClicked() {
        dialNumber();
    }

    @OnClick(R.id.btn_call_help) void onCallHelpClickedClicked() {
        dialNumber();
    }

    @OnClick(R.id.btn_call_ambulance) void onCallAmbulanceClicked() {
        dialNumber();
    }

    @Override
    public void onOrderClicked(int id, String pharmacy) {
        Intent intent = new Intent(this, OrderMedicineActivity.class);
        intent.putExtra("pharmacyId", id);
        intent.putExtra("pharmacyName", pharmacy);
        startActivity(intent);
    }

    @Override
    public void onGettingHomeDataSuccess(HomeData homeData) {
        VerticalItemsAdapter adapter = new VerticalItemsAdapter(this, homeData.categoryProducts, this);
        recyclerViewItems.setAdapter(adapter);
        recyclerViewItems.setLayoutManager(new LinearLayoutManager(this));

        HorizontalItemsAdapter popularProductsAdapter = new HorizontalItemsAdapter(this, homeData.popularProducts, this);
        recyclerViewPopularProducts.setAdapter(popularProductsAdapter);
        recyclerViewPopularProducts.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL, false));

        TopPharmaciesAdapter pharmaciesAdapter = new TopPharmaciesAdapter(this, homeData.pharmacies, this);
        recyclerViewTopPharmacies.setAdapter(pharmaciesAdapter);
        recyclerViewTopPharmacies.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        MainViewPagerAdapter adapter1 = new MainViewPagerAdapter(this, homeData.banners.sliders1);
        viewPager1.setAdapter(adapter1);

        MainViewPagerAdapter adapter2 = new MainViewPagerAdapter(this, homeData.banners.sliders2);
        viewPager2.setAdapter(adapter2);

        setViewPager1(homeData.banners.sliders1.length);
        setViewPager2(homeData.banners.sliders2.length);

        ArrayList<Image> bannerImages = new ArrayList<>();
        Image bannerImage1 = new Image(1, homeData.banners.banners[0].image);
        Image bannerImage2 = new Image(2, homeData.banners.banners[1].image);
        bannerImages.add(bannerImage1);
        bannerImages.add(bannerImage2);

        BannersViewpagerAdapter bannersViewpagerAdapter = new BannersViewpagerAdapter(this, bannerImages);
        viewPager3.setAdapter(bannersViewpagerAdapter);

        setViewPager3(bannerImages.size());

    }

    @Override
    public void loadCartCount(int cartCount) {
        if (cartCount > 0) {
            txtViewCartCount.setText(Integer.toString(cartCount));
            txtViewCartCount.setVisibility(View.VISIBLE);
        } else {
            txtViewCartCount.setVisibility(View.GONE);
        }
    }

    private void setViewPager1(int length) {
        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if(currentPage == length)
                {
                    currentPage = 0;
                }
                viewPager1.setCurrentItem(currentPage, true);
                currentPage++;
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 4000, 5000);
    }

    private void setViewPager2(int length) {
        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if(currentPage1 == length)
                {
                    currentPage1 = 0;
                }
                viewPager2.setCurrentItem(currentPage1, true);
                currentPage1++;
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 4000, 5000);
    }

    private void setViewPager3(int length) {
        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if(currentPage2 == length)
                {
                    currentPage2 = 0;
                }
                viewPager3.setCurrentItem(currentPage2, true);
                currentPage2++;
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 4000, 5000);
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }

    @OnClick(R.id.layout_health_products) void onHealthProductsClicked() {
        Intent intent = new Intent(this, SubcategoryActivity.class);
        intent.putExtra("categoryId", 2);
        startActivity(intent);
    }

    @OnClick(R.id.layout_order_medicine) void onOrderMedicineActivity() {
        Intent intent = new Intent(this, OrderMedicineActivity.class);
        startActivity(intent);
    }

    private void goToUserActivity() {
        Intent intent = new Intent(this, UserActivity.class);
        startActivity(intent);
    }

    @Override
    public void onProductClicked(int itemsId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", itemsId);
        startActivity(intent);
    }

    @Override
    public void onCategoryClicked(int categoryId) {
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra("categoryId", categoryId);
        intent.putExtra("type", 2);
        startActivity(intent);
    }

    private void callNumber() {
        String phone = "6000913778";
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.fromParts("tel", phone, null));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }

    private void dialNumber() {
        String phone = "6000913778";
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
        startActivity(intent);
    }

    private boolean checkAndRequestPermissions() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for(String perm: appPremisions){
            if(ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED){
                listPermissionsNeeded.add(perm);
            }
        }
        if(!listPermissionsNeeded.isEmpty()){
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0) {
                callNumber();
            } else {
                dialNumber();
            }
        }
    }

    @OnClick(R.id.btn_pharmacy_view_all) void onPharmacyViewAllClicked() {
        Intent intent = new Intent(this, PharmacyListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_featured_products_view_more) void onFeaturedProductsViewAllClicked() {
        Intent intent = new Intent(this, FetauredProductsListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_search) void onSearchClicked() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_cart) void onImageViewCartClicked() {
        if (isLoggedIn()) {
            goToCartActivity();
        } else {
            showLoginBottomSheet();
        }
    }

    private void goToCartActivity() {
        Intent intent = new Intent(this, CartListActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchCartDetails();
    }

}