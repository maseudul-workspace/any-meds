package com.webinfotech.anymeds.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.anymeds.R;
import com.webinfotech.anymeds.domain.executors.impl.ThreadExecutor;
import com.webinfotech.anymeds.domain.models.PharmacyListWrapper;
import com.webinfotech.anymeds.presentation.presenters.PharmacyListPresenter;
import com.webinfotech.anymeds.presentation.presenters.impl.PharmacyListPresenterImpl;
import com.webinfotech.anymeds.presentation.ui.adapters.PharmaciesMainAdapter;
import com.webinfotech.anymeds.threading.MainThreadImpl;

public class PharmacyListActivity extends AppCompatActivity implements PharmacyListPresenter.View {

    @BindView(R.id.recycler_view_pharmacy)
    RecyclerView recyclerViewPharmacy;
    PharmacyListPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pharmacy_list);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Pharmacies");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchPharmacies();
    }

    private void initialisePresenter() {
        mPresenter = new PharmacyListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(PharmaciesMainAdapter adapter) {
        recyclerViewPharmacy.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerViewPharmacy.setAdapter(adapter);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onOrderClicked(int pharmacyId, String pharmacy) {
        Intent intent = new Intent(this, OrderMedicineActivity.class);
        intent.putExtra("pharmacyId", pharmacyId);
        intent.putExtra("pharmacyName", pharmacy);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}