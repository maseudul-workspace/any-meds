package com.webinfotech.anymeds.presentation.presenters;

import com.webinfotech.anymeds.presentation.ui.adapters.PharmaciesMainAdapter;

public interface PharmacyListPresenter {
    void fetchPharmacies();
    interface View {
        void loadAdapter(PharmaciesMainAdapter adapter);
        void showLoader();
        void hideLoader();
        void onOrderClicked(int pharmacyId, String pharmacy);
    }
}
