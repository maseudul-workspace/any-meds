package com.webinfotech.anymeds.presentation.ui.activities;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.navigation.NavigationView;
import com.webinfotech.anymeds.R;

public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) @Nullable
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    @BindView(R.id.navigation)
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }

    protected void inflateContent(@LayoutRes int inflateResID){
        setContentView(R.layout.activity_base);
        FrameLayout contentFramelayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflateResID, contentFramelayout);
        ButterKnife.bind(this);
        setToolbar();
        setUpNavigationView();
    }

    public void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    public void setUpNavigationView() {
        Menu navMenu = navigationView.getMenu();
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.nav_order_medicine:
                        goToOrderMedicineActivity();
                        break;
                    case R.id.nav_order_health_products:
                        goToHealthProducts();
                        break;
                    case R.id.nav_doctor_appointment:
                        goToMyDoctorActivity();
                        break;
                    case R.id.nav_labs_diagnostics:
                        goToLabDiagsonsticsActivity();
                        break;
                    case R.id.nav_ambulance_service:
                        goToAmbulanceServiceActivity();
                        break;
                    case R.id.nav_seller_csp:
                        goToSellerCspActivity();
                        break;
                }
                return false;
            }
        });
    }

    private void goToOrderMedicineActivity() {
        Intent intent = new Intent(this, OrderMedicineActivity.class);
        startActivity(intent);
    }

    private void goToHealthProducts() {
        Intent intent = new Intent(this, SubcategoryActivity.class);
        intent.putExtra("categoryId", 2);
        startActivity(intent);
    }

    private void goToMyDoctorActivity() {
        Intent intent = new Intent(this, MyDoctorsActivity.class);
        startActivity(intent);
    }

    private void goToLabDiagsonsticsActivity() {
        Intent intent = new Intent(this, LabDiagnosticsActivity.class);
        startActivity(intent);
    }

    private void goToAmbulanceServiceActivity() {
        Intent intent = new Intent(this, AmbulanceServiceActivity.class);
        startActivity(intent);
    }

    private void goToSellerCspActivity() {
        Intent intent = new Intent(this, SellerAndCspActivity.class);
        startActivity(intent);
    }

}