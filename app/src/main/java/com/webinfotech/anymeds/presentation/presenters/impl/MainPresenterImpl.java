package com.webinfotech.anymeds.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.anymeds.AndroidApplication;
import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.FetchHomeDataInteractor;
import com.webinfotech.anymeds.domain.interactors.impl.FetchCartListInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.FetchHomeDataInteractorImpl;
import com.webinfotech.anymeds.domain.models.CartList;
import com.webinfotech.anymeds.domain.models.HomeData;
import com.webinfotech.anymeds.domain.models.UserInfo;
import com.webinfotech.anymeds.presentation.presenters.MainPresenter;
import com.webinfotech.anymeds.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter,
                                                                    FetchHomeDataInteractor.Callback,
                                                                    FetchCartListInteractorImpl.Callback

{

    Context mContext;
    MainPresenter.View mView;

    public MainPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchHomeData() {
        FetchHomeDataInteractorImpl fetchHomeDataInteractor = new FetchHomeDataInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl());
        fetchHomeDataInteractor.execute();
    }

    @Override
    public void fetchCartDetails() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            FetchCartListInteractorImpl fetchCartListInteractor = new FetchCartListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchCartListInteractor.execute();
        }
    }

    @Override
    public void onGettingHomeDataSuccess(HomeData homeData) {
        mView.hideLoader();
        mView.onGettingHomeDataSuccess(homeData);
    }

    @Override
    public void onGettingHomeDataFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onGettingCartListSuccess(CartList[] cartLists) {
        mView.loadCartCount(cartLists.length);
    }

    @Override
    public void onGettingCartListFail(String errorMsg, int loginError) {
        mView.loadCartCount(0);
    }
}
