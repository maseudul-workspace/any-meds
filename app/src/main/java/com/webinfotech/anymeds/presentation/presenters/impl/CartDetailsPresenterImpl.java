package com.webinfotech.anymeds.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import com.webinfotech.anymeds.AndroidApplication;
import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.FetchChargesListInteractor;
import com.webinfotech.anymeds.domain.interactors.FetchShippingAddressInteractor;
import com.webinfotech.anymeds.domain.interactors.GetWalletDetailsInteractor;
import com.webinfotech.anymeds.domain.interactors.InitiatePaymentInteractor;
import com.webinfotech.anymeds.domain.interactors.PlaceOrderInteractor;
import com.webinfotech.anymeds.domain.interactors.RemoveCartItemInteractor;
import com.webinfotech.anymeds.domain.interactors.RequestPaymentInteractor;
import com.webinfotech.anymeds.domain.interactors.SetTransactionIdInteractor;
import com.webinfotech.anymeds.domain.interactors.UpdateCartInteractor;
import com.webinfotech.anymeds.domain.interactors.VerifyPaymentInteractor;
import com.webinfotech.anymeds.domain.interactors.impl.FetchCartListInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.FetchChargesListInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.FetchShippingAddressInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.GetWalletDetailsInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.InitiatePaymentInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.PlaceOrderInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.RemoveCartItemInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.RequestPaymentInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.SetTransactionIdInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.UpdateCartInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.VerifyPaymentInteractorImpl;
import com.webinfotech.anymeds.domain.models.CartList;
import com.webinfotech.anymeds.domain.models.Charges;
import com.webinfotech.anymeds.domain.models.GatewayOrder;
import com.webinfotech.anymeds.domain.models.OrderPlaceData;
import com.webinfotech.anymeds.domain.models.PaymentRequestResponse;
import com.webinfotech.anymeds.domain.models.Product;
import com.webinfotech.anymeds.domain.models.ShippingAddress;
import com.webinfotech.anymeds.domain.models.Size;
import com.webinfotech.anymeds.domain.models.UserInfo;
import com.webinfotech.anymeds.domain.models.WalletDetails;
import com.webinfotech.anymeds.presentation.presenters.CartDetailsPresenter;
import com.webinfotech.anymeds.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.anymeds.presentation.ui.adapters.CartListAdapter;
import com.webinfotech.anymeds.presentation.ui.adapters.CartShippingAddressAdapter;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;
import com.webinfotech.anymeds.repository.PaymentRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class CartDetailsPresenterImpl extends AbstractPresenter implements CartDetailsPresenter,
                                                                            CartShippingAddressAdapter.Callback,
                                                                            FetchShippingAddressInteractor.Callback,
                                                                            FetchCartListInteractorImpl.Callback,
                                                                            CartListAdapter.Callback,
                                                                            UpdateCartInteractor.Callback,
                                                                            RemoveCartItemInteractor.Callback,
                                                                            FetchChargesListInteractor.Callback,
                                                                            GetWalletDetailsInteractor.Callback,
                                                                            PlaceOrderInteractor.Callback,
                                                                            InitiatePaymentInteractor.Callback,
                                                                            RequestPaymentInteractor.Callback,
                                                                            SetTransactionIdInteractor.Callback,
                                                                            VerifyPaymentInteractor.Callback
{

    Context mContext;
    CartDetailsPresenter.View mView;
    FetchShippingAddressInteractorImpl fetchShippingAddressInteractor;
    ShippingAddress[] shippingAddresses;
    CartShippingAddressAdapter cartShippingAddressAdapter;
    FetchCartListInteractorImpl fetchCartListInteractor;
    UpdateCartInteractorImpl updateCartInteractor;
    RemoveCartItemInteractorImpl removeCartItemInteractor;
    String accessToken;
    int orderId;
    int paymentMethod;

    public CartDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchCartDetails() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            mView.showLoginBottomSheet();
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            fetchCartListInteractor = new FetchCartListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchCartListInteractor.execute();
        }
    }

    @Override
    public void fetchShippingAddressList() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            mView.showLoginBottomSheet();
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            fetchShippingAddressInteractor = new FetchShippingAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchShippingAddressInteractor.execute();
        }
    }

    @Override
    public void fetchChargesList() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
        } else {
            FetchChargesListInteractorImpl fetchChargesListInteractor = new FetchChargesListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
            fetchChargesListInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void getWalletDetails() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
        } else {
            GetWalletDetailsInteractorImpl getWalletDetailsInteractor = new GetWalletDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            getWalletDetailsInteractor.execute();
        }
    }

    @Override
    public void placeOrder(int paymentMethod, int addressId, int orderType, int isWallet, String prescription) {
        this.paymentMethod = paymentMethod;
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            mView.showLoginBottomSheet();
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            PlaceOrderInteractorImpl placeOrderInteractor = new PlaceOrderInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, paymentMethod, addressId, orderType, isWallet, 0, "", prescription);
            placeOrderInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void initiatePayment(String accessToken, String amount, String transactionId, String env) {
        this.accessToken = accessToken;
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        InitiatePaymentInteractorImpl initiatePaymentInteractor = new InitiatePaymentInteractorImpl(mExecutor, mMainThread, new PaymentRepositoryImpl(), this, accessToken, user.name, "razthemonster@gmail.com", user.mobile, amount, "", "INR", transactionId, "https://test.instamojo.com/integrations/android/redirect/", env);
        initiatePaymentInteractor.execute();
    }

    @Override
    public void requestPayment(String accessToken, String id) {
        RequestPaymentInteractorImpl requestPaymentInteractor = new RequestPaymentInteractorImpl(mExecutor, mMainThread, new PaymentRepositoryImpl(), this, accessToken, id);
        requestPaymentInteractor.execute();
    }

    @Override
    public void setTransactionId(int orderId, String transactionId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        SetTransactionIdInteractorImpl setTransactionIdInteractor = new SetTransactionIdInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, orderId, transactionId);
        setTransactionIdInteractor.execute();
    }

    @Override
    public void verifyPayment(String instamojoOrderId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        VerifyPaymentInteractorImpl verifyPaymentInteractor = new VerifyPaymentInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, this.orderId, instamojoOrderId);
        verifyPaymentInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onGettingAddressListSucces(ShippingAddress[] shippingAddresses) {
        this.shippingAddresses = shippingAddresses;
        if (this.shippingAddresses.length > 0) {
            this.shippingAddresses[0].isSelected = true;
            mView.onAddressSelected(this.shippingAddresses[0].id);
        }
        cartShippingAddressAdapter = new CartShippingAddressAdapter(mContext, this.shippingAddresses, this);
        mView.loadShippingAddressAdapter(cartShippingAddressAdapter);
    }

    @Override
    public void onGettingAddressListFail(String errorMsg, int loginError) {

    }

    @Override
    public void onAddressSelected(int id) {
        for (int i = 0; i < shippingAddresses.length; i++) {
            if (shippingAddresses[i].id == id) {
                shippingAddresses[i].isSelected = true;
            } else {
                shippingAddresses[i].isSelected = false;
            }
        }
        cartShippingAddressAdapter.updateDataSet(this.shippingAddresses);
        mView.onAddressSelected(id);
    }

    @Override
    public void onEditClicked(int id) {
        mView.onShippingAddressEditClicked(id);
    }

    @Override
    public void onDeliverButtonClicked() {
        mView.onDeliverButtonClicked();
    }

    @Override
    public void onGettingCartListSuccess(CartList[] cartLists) {
        if (cartLists.length == 0) {
            mView.hideViews();
        } else {
            double total = 0;
            double discount = 0;
            double subTotal = 0;
            for (int position = 0; position < cartLists.length; position++) {
                Size[] sizes = cartLists[position].product.sizes;
                for (int i = 0; i < sizes.length; i++) {
                    if (sizes[i].id == cartLists[position].sizeId) {
                        total = total + sizes[i].price * cartLists[position].quantity;
                        subTotal = subTotal + sizes[i].mrp * cartLists[position].quantity;
                        discount = subTotal - total;
                        break;
                    }
                }
            }
            boolean prescriptionFlag = false;
            for (int position = 0; position < cartLists.length; position++) {
                Product product = cartLists[position].product;
                if (product.isPrescription == 2) {
                    prescriptionFlag = true;
                    break;
                }
            }
            CartListAdapter adapter = new CartListAdapter(mContext, cartLists, this);
            mView.loadCartAdapter(adapter, total, discount, subTotal);
            mView.changePrescriptionLayoutVisibility(prescriptionFlag);
        }
        if (cartLists.length == 1 && cartLists[0].quantity > 6) {
            mView.hideExpressDeliveryOption();
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingCartListFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.hideViews();
        if (loginError == 1) {
            Toasty.warning(mContext, "Session expired !! Please Login Again").show();
            mView.showLoginBottomSheet();
        } else {
            Toasty.warning(mContext, errorMsg).show();
        }
    }

    @Override
    public void updateCart(int quantity, int cartId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            updateCartInteractor = new UpdateCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, cartId, quantity);
            updateCartInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void removeCart(int cartId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            removeCartItemInteractor = new RemoveCartItemInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, cartId);
            removeCartItemInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void goToProductDetails(int productId) {
        Log.e("LogMsg", "Product Id: " + productId);
        mView.goToProductDetails(productId);
    }

    @Override
    public void onCartUpdateSuccess() {
        Toasty.success(mContext, "Updated Successfully").show();
		fetchCartDetails();
    }

    @Override
    public void onCartUpdateFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onCartItemRemoveSuccess() {
        fetchCartDetails();
        Toasty.success(mContext, "Item Removed Successfully").show();
    }

    @Override
    public void onCartItemRemoveFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onChargesListFetchSuccess(Charges[] charges) {
        mView.loadCharges(charges);
    }

    @Override
    public void onChargesListFetchFail() {

    }

    @Override
    public void onGettingCreditHistorySuccess(WalletDetails walletDetails) {
        mView.loadWalletDetails(walletDetails.totalAmount);
    }

    @Override
    public void onGettingCreditHistoryFail(String errorMsg, int loginError) {

    }

    @Override
    public void onPlaceOrderSuccess(OrderPlaceData orderPlaceData) {
        if (this.paymentMethod == 2) {
            this.orderId = orderPlaceData.order.id;
            mView.loadPaymentData(orderPlaceData);
        } else {
            mView.hideLoader();
            mView.goToCodPaymentResponse(String.format("%.2f", orderPlaceData.order.amount));
        }

    }

    @Override
    public void onPlaceOrderFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onInitiatePaymentSuccess(GatewayOrder gatewayOrder) {
        requestPayment(accessToken, gatewayOrder.id);
    }

    @Override
    public void onInitiatePaymentFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onRequestPaymentSuccess(PaymentRequestResponse paymentRequestResponse) {
        setTransactionId(orderId, paymentRequestResponse.orderId);
        mView.setOrderId(paymentRequestResponse.orderId);
        mView.hideLoader();
    }

    @Override
    public void onRequestPaymentFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onSetTransactionIdSuccess() {

    }

    @Override
    public void onSetTransactionIdFail(String errorMsg, int loginError) {

    }

    @Override
    public void onVerifyPaymentSuccess() {
        mView.hideLoader();
        mView.onPaymentVerifySuccess();
    }

    @Override
    public void onVerifyPaymentFail() {
        mView.hideLoader();
        mView.onPaymentVerifyFail();
    }
}
