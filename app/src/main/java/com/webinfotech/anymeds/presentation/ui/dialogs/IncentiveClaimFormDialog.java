package com.webinfotech.anymeds.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import com.webinfotech.anymeds.R;
import com.webinfotech.anymeds.util.GlideHelper;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class IncentiveClaimFormDialog {

    public interface Callback {
        void submitIncentiveClaimForm(String cspIdNo, String phoneNo, String fileName);
        void onImageClicked();
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.edit_text_csp_id_no)
    EditText editTextCspIdNo;
    @BindView(R.id.edit_text_phone_no)
    EditText edit_text_phone_no;
    @BindView(R.id.img_view_prescription)
    ImageView imgViewPrescription;
    @BindView(R.id.check_box_agreement)
    CheckBox checkBoxAgreement;
    String fileName;

    public IncentiveClaimFormDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_icentive_claim_form, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    @OnClick(R.id.layout_prescription) void onFileClicked() {
        mCallback.onImageClicked();
    }

    @OnClick(R.id.img_view_cancel) void onCancelClicked() {
        hideDialog();
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (    edit_text_phone_no.getText().toString().trim().isEmpty() ||
                editTextCspIdNo.getText().toString().trim().isEmpty()
        ) {
            Toasty.warning(mContext, "Some required fields are empty", Toasty.LENGTH_SHORT).show();
        } else if (fileName == null) {
            Toasty.warning(mContext, "Please upload basis of incentive", Toasty.LENGTH_SHORT).show();
        } else if (!checkBoxAgreement.isChecked()) {
            Toasty.warning(mContext, "Please check the terms and condition", Toasty.LENGTH_SHORT).show();
        } else {
            mCallback.submitIncentiveClaimForm(     editTextCspIdNo.getText().toString(),
                                                    edit_text_phone_no.getText().toString(),
                                                    fileName
            );
        }
    }

    public void setImage(String image) {
        fileName = image;
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imgViewPrescription, image, 100);
    }

}
