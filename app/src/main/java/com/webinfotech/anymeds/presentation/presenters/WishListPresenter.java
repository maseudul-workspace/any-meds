package com.webinfotech.anymeds.presentation.presenters;

import com.webinfotech.anymeds.presentation.ui.adapters.WishlistAdapter;

public interface WishListPresenter {
    void fetchWishList();
    interface View {
        void loadAdapter(WishlistAdapter adapter);
        void onProductClicked(int productId);
        void showLoader();
        void hideLoader();
    }
}
