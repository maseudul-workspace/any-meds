package com.webinfotech.anymeds.presentation.presenters;

public interface ForgetPasswordPresenter {
    void requestPasswordChange(String mobile, String password);
    interface View {
        void onPasswordRequestSuccess();
        void showLoader();
        void hideLoader();
    }
}
