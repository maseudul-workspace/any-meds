package com.webinfotech.anymeds.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.webinfotech.anymeds.R;
import com.webinfotech.anymeds.domain.executors.impl.ThreadExecutor;
import com.webinfotech.anymeds.presentation.presenters.SearchListPresenter;
import com.webinfotech.anymeds.presentation.presenters.impl.SearchListPresenterImpl;
import com.webinfotech.anymeds.presentation.ui.adapters.SearchListAdapter;
import com.webinfotech.anymeds.threading.MainThreadImpl;

public class SearchActivity extends AppCompatActivity implements SearchListPresenter.View {

    SearchView searchView;
    SearchListPresenterImpl mPresenter;
    @BindView(R.id.recycler_view_search_list)
    RecyclerView recyclerViewSearchList;
    @BindView(R.id.layout_request_medicine)
    View layoutRequestMedicine;
    @BindView(R.id.txt_view_medicine_name)
    TextView txtViewMedicineName;
    @BindView(R.id.txt_view_medicine_desc)
    TextView txtViewMedicineDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Search Medicine");
        initialisePresenter();
    }

    private void initialisePresenter() {
        mPresenter = new SearchListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate( R.menu.search_menu, menu);

        MenuItem myActionMenuItem = menu.findItem( R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        myActionMenuItem.expandActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(!query.isEmpty()) {
                    mPresenter.fetchSearchList(query);
                }
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                if(!s.isEmpty()){
                    mPresenter.fetchSearchList(s);
                }
                return false;
            }
        });
        return true;
    }

    @Override
    public void loadAdapter(SearchListAdapter adapter) {
        recyclerViewSearchList.setAdapter(adapter);
        recyclerViewSearchList.setLayoutManager(new LinearLayoutManager(this));

        recyclerViewSearchList.setVisibility(View.VISIBLE);
        layoutRequestMedicine.setVisibility(View.GONE);
    }

    @Override
    public void onSearchNotFound(String searchKey) {
        recyclerViewSearchList.setVisibility(View.GONE);
        layoutRequestMedicine.setVisibility(View.VISIBLE);
        txtViewMedicineName.setText("Couldn't find " + searchKey);
        txtViewMedicineDesc.setText("Don’t worry! You can still order and we will check manually for availability of " + searchKey + ". Click Here To Order !!");
    }

    @Override
    public void onProductClicked(int id) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", id);
        startActivity(intent);
    }

    @OnClick(R.id.layout_request_medicine) void onRequestMedicineClicked() {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send?phone=+916000913778&text=I want to order a Medicine/Health product"));
        startActivity(link);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}