package com.webinfotech.anymeds.presentation.presenters;

public interface ChangePasswordPresenter {
    void changePassword(String oldPassword, String newPassword);
    interface View {
        void showLoader();
        void hideLoader();
    }
}
