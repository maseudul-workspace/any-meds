package com.webinfotech.anymeds.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;


import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.ResetPasswordInteractor;
import com.webinfotech.anymeds.domain.interactors.impl.ResetPasswordInteractorImpl;
import com.webinfotech.anymeds.presentation.presenters.ForgetPasswordPresenter;
import com.webinfotech.anymeds.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ForgetPasswordPresenterImpl extends AbstractPresenter implements ForgetPasswordPresenter, ResetPasswordInteractor.Callback {

    Context mContext;
    ForgetPasswordPresenter.View mView;

    public ForgetPasswordPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void onResetPasswordSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Password Changed").show();
        mView.onPasswordRequestSuccess();
    }

    @Override
    public void onResetPasswordFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void requestPasswordChange(String mobile, String password) {
        ResetPasswordInteractorImpl resetPasswordInteractor = new ResetPasswordInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, mobile, password);
        resetPasswordInteractor.execute();
    }
}
