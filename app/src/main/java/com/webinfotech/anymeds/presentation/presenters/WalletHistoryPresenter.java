package com.webinfotech.anymeds.presentation.presenters;

import com.webinfotech.anymeds.domain.models.WalletDetails;
import com.webinfotech.anymeds.presentation.ui.adapters.WalletHistoryAdapter;

public interface WalletHistoryPresenter {
    void fetchWalletHistory();
    interface View {
        void loadAdapter(WalletHistoryAdapter adapter, WalletDetails walletDetails);
        void showLoader();
        void hideLoader();
        void showLoginBottomSheet();
    }
}
