package com.webinfotech.anymeds.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.anymeds.AndroidApplication;
import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.AddToWishListInteractor;
import com.webinfotech.anymeds.domain.interactors.FetchFeaturedProductsInteractor;
import com.webinfotech.anymeds.domain.interactors.FetchWishListInteractor;
import com.webinfotech.anymeds.domain.interactors.RemoveFromWishlistInteractor;
import com.webinfotech.anymeds.domain.interactors.impl.AddToWishListInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.FetchFeaturedProductsInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.FetchWishListInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.RemoveFromWishlistInteractorImpl;
import com.webinfotech.anymeds.domain.models.Product;
import com.webinfotech.anymeds.domain.models.UserInfo;
import com.webinfotech.anymeds.domain.models.WishList;
import com.webinfotech.anymeds.presentation.presenters.FeaturedProductsPresenter;
import com.webinfotech.anymeds.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.anymeds.presentation.ui.adapters.ProductsVerticalAdapter;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class FeaturedProductsPresenterImpl extends AbstractPresenter implements FeaturedProductsPresenter,
                                                                                FetchFeaturedProductsInteractor.Callback,
                                                                                ProductsVerticalAdapter.Callback,
                                                                                AddToWishListInteractor.Callback,
                                                                                FetchWishListInteractor.Callback,
                                                                                RemoveFromWishlistInteractor.Callback
{

    Context mContext;
    FeaturedProductsPresenter.View mView;
    private WishList[] wishLists;
    private int position;
    ProductsVerticalAdapter adapter;

    public FeaturedProductsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchFeaturedProducts() {
        FetchFeaturedProductsInteractorImpl fetchFeaturedProductsInteractor = new FetchFeaturedProductsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchFeaturedProductsInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void fetchWishList() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            fetchFeaturedProducts();
        } else {
            FetchWishListInteractorImpl fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchWishListInteractor.execute();
        }
    }

    @Override
    public void onFeaturedProductsFetchSuccess(Product[] products) {
        mView.hideLoader();
        if (wishLists != null) {
            for (int i = 0; i < wishLists.length; i++) {
                for (int j = 0; j < products.length; j++) {
                    if (products[j].id == wishLists[i].product.id) {
                        products[j].isWishListed = true;
                    }
                }
            }
        }
        adapter = new ProductsVerticalAdapter(mContext, products, this);
        mView.loadAdapter(adapter);
    }

    @Override
    public void onFeaturedProductsFetchFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProductDetails(productId);
    }

    @Override
    public void addToWishList(int productId, int position) {
        this.position = position;
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            mView.showLoginBottomSheet();
        } else {
            AddToWishListInteractorImpl addToWishListInteractor = new AddToWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId, productId);
            addToWishListInteractor.execute();
        }
    }

    @Override
    public void removeFromWishList(int productId, int position) {
        this.position = position;
        int wishListId = 0;
        for (int i = 0; i < wishLists.length; i++) {
            if (wishLists[i].product.id == productId) {
                wishListId = wishLists[i].wishListId;
                break;
            }
        }
        this.position = position;
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            mView.showLoginBottomSheet();
        } else {
            RemoveFromWishlistInteractorImpl removeFromWishlistInteractor = new RemoveFromWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, wishListId);
            removeFromWishlistInteractor.execute();
        }
    }

    @Override
    public void onAddToWishListSuccess() {
        Toasty.normal(mContext, "Added To Wish List").show();
        adapter.onAddWishListSuccess(position);
    }

    @Override
    public void onAddToWishListFail(String errorMsg, int loginError) {

    }

    @Override
    public void onWishListFetchSuccess(WishList[] wishLists) {
        this.wishLists = wishLists;
        fetchFeaturedProducts();
    }

    @Override
    public void onWishListFetchFail(String errorMsg, int loginError) {
        fetchFeaturedProducts();
    }

    @Override
    public void onWishListRemoveSuccess() {
        Toasty.normal(mContext, "Removed From Wishlist").show();
        adapter.onWishListRemoveSuccess(position);
    }

    @Override
    public void onWishListRemoveFail(String errorMsg, int loginError) {

    }
}
