package com.webinfotech.anymeds.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.ClaimIncentiveInteractor;
import com.webinfotech.anymeds.domain.interactors.RegisterCspInteractor;
import com.webinfotech.anymeds.domain.interactors.RegisterSellerInteractor;
import com.webinfotech.anymeds.domain.interactors.base.AbstractInteractor;
import com.webinfotech.anymeds.domain.interactors.impl.ClaimIncentiveInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.RegisterCspInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.RegisterSellerinteractorImpl;
import com.webinfotech.anymeds.presentation.presenters.SellerAndCspPresenter;
import com.webinfotech.anymeds.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class SellerAndCspPresenterImpl extends AbstractPresenter implements SellerAndCspPresenter,
                                                                            RegisterCspInteractor.Callback,
                                                                            RegisterSellerInteractor.Callback,
                                                                            ClaimIncentiveInteractor.Callback
{

    Context mContext;
    SellerAndCspPresenter.View mView;

    public SellerAndCspPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void registerCsp(String applicantName, String pharmacyName, String address, String pin, String phoneNo, String email, String phonePe, String bankAC, String ifsc) {
        RegisterCspInteractorImpl registerCspInteractor = new RegisterCspInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, applicantName, pharmacyName, phoneNo, phonePe, pin, address, bankAC, ifsc, email);
        registerCspInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void registerSeller(String pharmacyName, String dlNo, String gstNo, String name, String ownerContactNo, String whatsappNo, String address, String pin, String email, String phonePe, String bankAC, String ifsc) {
        RegisterSellerinteractorImpl registerSellerinteractor = new RegisterSellerinteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, pharmacyName, dlNo, gstNo, name, ownerContactNo, whatsappNo, phonePe, email, pin, address, bankAC, ifsc);
        registerSellerinteractor.execute();
        mView.showLoader();
    }

    @Override
    public void claimIncentive(String cspIdNo, String phoneNo, String fileName) {
        ClaimIncentiveInteractorImpl claimIncentiveInteractor = new ClaimIncentiveInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, cspIdNo, phoneNo, fileName);
        claimIncentiveInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onRegisterCspSuccess() {
        mView.showSnackBar("Registered Successfully !! You will get call from our executive shortly");
        mView.hideLoader();
    }

    @Override
    public void onRegisterCspFail(String errorMsg) {
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
        mView.hideLoader();
    }

    @Override
    public void onIncentiveClaimSuccess() {
        mView.showSnackBar("Claimed Successfully");
        mView.hideLoader();
    }

    @Override
    public void onIncentiveClaimFail(String errorMsg) {
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
        mView.hideLoader();
    }

    @Override
    public void onRegisterSellerSuccess() {
        mView.showSnackBar("Registered Successfully !! You will get call from our executive shortly");
        mView.hideLoader();
    }

    @Override
    public void onRegisterSellerFail(String errorMsg) {
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
        mView.hideLoader();
    }
}
