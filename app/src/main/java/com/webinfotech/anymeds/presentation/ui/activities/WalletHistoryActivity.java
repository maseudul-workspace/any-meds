package com.webinfotech.anymeds.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.webinfotech.anymeds.R;
import com.webinfotech.anymeds.domain.executors.impl.ThreadExecutor;
import com.webinfotech.anymeds.domain.models.WalletDetails;
import com.webinfotech.anymeds.presentation.presenters.WalletHistoryPresenter;
import com.webinfotech.anymeds.presentation.presenters.impl.WalletHistoryPresenterImpl;
import com.webinfotech.anymeds.presentation.ui.adapters.WalletHistoryAdapter;
import com.webinfotech.anymeds.presentation.ui.bottomsheets.LoginBottomSheet;
import com.webinfotech.anymeds.threading.MainThreadImpl;

public class WalletHistoryActivity extends AppCompatActivity implements WalletHistoryPresenter.View {

    @BindView(R.id.recycler_view_wallet_history)
    RecyclerView recyclerViewWalletHistory;
    @BindView(R.id.txt_view_total_amount)
    TextView txtViewWalletAmount;
    WalletHistoryPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_history);
        getSupportActionBar().setTitle("Wallet");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        setProgressDialog();
        initialisePresenter();
        mPresenter.fetchWalletHistory();
    }

    private void initialisePresenter() {
        mPresenter = new WalletHistoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(WalletHistoryAdapter adapter, WalletDetails walletDetails) {
        txtViewWalletAmount.setText("₹ " + walletDetails.totalAmount);
        recyclerViewWalletHistory.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewWalletHistory.setAdapter(adapter);
    }


    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void showLoginBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}