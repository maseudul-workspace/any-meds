package com.webinfotech.anymeds.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.anymeds.AndroidApplication;
import com.webinfotech.anymeds.domain.executors.Executor;
import com.webinfotech.anymeds.domain.executors.MainThread;
import com.webinfotech.anymeds.domain.interactors.DeleteShippingAddressInteractor;
import com.webinfotech.anymeds.domain.interactors.FetchShippingAddressInteractor;
import com.webinfotech.anymeds.domain.interactors.impl.DeleteShippingAddressInteractorImpl;
import com.webinfotech.anymeds.domain.interactors.impl.FetchShippingAddressInteractorImpl;
import com.webinfotech.anymeds.domain.models.ShippingAddress;
import com.webinfotech.anymeds.domain.models.UserInfo;
import com.webinfotech.anymeds.presentation.presenters.ShippingAddressPresenter;
import com.webinfotech.anymeds.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.anymeds.presentation.ui.adapters.ShippingAddressAdapter;
import com.webinfotech.anymeds.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ShippingAddressPresenterImpl extends AbstractPresenter implements ShippingAddressPresenter,
                                                                                FetchShippingAddressInteractor.Callback,
                                                                                ShippingAddressAdapter.Callback,
                                                                                DeleteShippingAddressInteractor.Callback
{

    Context mContext;
    ShippingAddressPresenter.View mView;
    FetchShippingAddressInteractorImpl fetchShippingAddressInteractor;
    DeleteShippingAddressInteractorImpl deleteShippingAddressInteractor;

    public ShippingAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchShippingAddress() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            fetchShippingAddressInteractor = new FetchShippingAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchShippingAddressInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingAddressListSucces(ShippingAddress[] shippingAddresses) {
        ShippingAddressAdapter shippingAddressAdapter = new ShippingAddressAdapter(mContext, shippingAddresses, this);
        mView.loadAdapter(shippingAddressAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingAddressListFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.onAddressFetchFailed();
    }

    @Override
    public void onEditClicked(int id) {
        mView.goToAddressDetails(id);
    }

    @Override
    public void onDeleteClicked(int id) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            deleteShippingAddressInteractor = new DeleteShippingAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, id, user.apiToken);
            deleteShippingAddressInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onAddressDeleteSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Address Deleted Successfully").show();
        fetchShippingAddress();
    }

    @Override
    public void onAddressDeleteFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            Toasty.warning(mContext, "Session expired !! Please Login Again").show();
            mView.showLoginBottomSheet();
        } else {
            Toasty.warning(mContext, errorMsg).show();
        }
    }
}
