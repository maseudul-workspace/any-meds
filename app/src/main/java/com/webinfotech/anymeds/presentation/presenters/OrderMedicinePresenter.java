package com.webinfotech.anymeds.presentation.presenters;

import com.webinfotech.anymeds.presentation.ui.adapters.CartShippingAddressAdapter;

public interface OrderMedicinePresenter {
    void fetchShippingAddressList();
    void placeOrder(String filePath, String pharmacyName, int pharmacyId);
    interface View {
        void loadShippingAddressAdapter(CartShippingAddressAdapter cartShippingAddressAdapter);
        void onShippingAddressEditClicked(int addressId);
        void goToOrderHistory(String medicine);
        void showLoader();
        void hideLoader();
        void onDeliverButtonClicked();
    }
}
