package com.webinfotech.anymeds.presentation.presenters;

import com.webinfotech.anymeds.domain.models.OrderPlaceData;

public interface PaymentPresenter {
    void placeOrder(int paymentMethod, int addressId, int orderType, int isWallet, String prescription);
    interface View {
        void loadPaymentData(OrderPlaceData orderPlaceData);
        void showLoader();
        void hideLoader();
    }
}
