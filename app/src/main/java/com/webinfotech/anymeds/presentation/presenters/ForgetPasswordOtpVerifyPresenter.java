package com.webinfotech.anymeds.presentation.presenters;

public interface ForgetPasswordOtpVerifyPresenter {
    void sendOtp(String phone);
    interface View {
        void onOtpSendSuccess(String otp);
        void showLoader();
        void hideLoader();
    }
}
